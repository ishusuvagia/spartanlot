
#import "AccountModel.h"

@implementation AccountModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"userId": @"Userid",
            @"token": @"Token",
            @"success": @"Success",
            @"name": @"Name",
            @"migrate": @"Migrate",
            @"companyId": @"Companyid",
            @"transactionId": @"TransactionId",
            @"errorMessage": @"ErrorMessage",
            @"errorCode": @"ErrorCode"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.userId > 0 && self.token != nil && self.success == YES;
}

@end
