
#import "ObjectModel.h"
#import "Utils.h"

@implementation ObjectModel

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)dotnetDateTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        NSDate *date = [Utils convertDotNetDateToNSDate:dateString];
        return date;
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        NSString *dateString = [Utils convertNSDateToDotNetDate:date];
        return dateString;
    }];
}

+ (NSValueTransformer *)dateTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        NSDate *date = [Utils convertISO8601StringtoNSDate:dateString];
        if (!date) date = [Utils convertStringToNSDate:dateString withFormat:kFormatDateISO8601Short];
        return date;
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        NSString *dateString = [Utils convertNSDatetoISO8601String:date];
        if (!dateString) dateString = [Utils convertNSDateToString:date withFormat:kFormatDateISO8601Short];
        return dateString;
    }];
}

+ (NSValueTransformer *)dateUTCTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [Utils convertStringToNSDate:dateString withFormat:kFormatDateISO8601UTC];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [Utils convertNSDateToString:date withFormat:kFormatDateISO8601UTC];
    }];
}

+ (NSValueTransformer *)timeTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [Utils convertStringToNSDate:dateString withFormat:k24FormatTimeHourMinute];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [Utils convertNSDateToString:date withFormat:k24FormatTimeHourMinute];
    }];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (BOOL)isValid
{
    // This will be implemented at subclass
    return NO;
}

@end
