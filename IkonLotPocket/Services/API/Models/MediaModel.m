
#import "MediaModel.h"

@interface MediaModel ()

@property (nonatomic, strong) NSMutableArray *_noDuplicatePhotoLinks;

@end

@implementation MediaModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"photoLinks": @"photo_links"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return YES;
}

///--------------------------------------
#pragma mark - Getter
///--------------------------------------

- (NSArray *)photoLinks
{
    if (!__noDuplicatePhotoLinks) {
        __noDuplicatePhotoLinks = [NSMutableArray array];
    }
    [self._noDuplicatePhotoLinks removeAllObjects];
    for (NSString *photoLink in _photoLinks) {
        if (![self._noDuplicatePhotoLinks containsObject:photoLink]) {
            [self._noDuplicatePhotoLinks addObject:photoLink];
        }
    }
    return self._noDuplicatePhotoLinks;
}

@end
