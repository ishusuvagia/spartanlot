
#import "DeviceLocateModel.h"

@implementation DeviceLocateContainerModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"result": @"locateResult"
    };
}

///--------------------------------------
#pragma mark - Transformer
///--------------------------------------

+ (NSValueTransformer *)resultJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:DeviceLocateModel.class];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.result != nil;
}

@end


@implementation DeviceLocateModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"errorCode": @"ErrorCode",
            @"errorMessage": @"ErrorMessage",
            @"transactionId": @"TransactionId"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.errorCode == 0;
}

@end
