
#import "DeviceModel.h"

@implementation DeviceDetailModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"deviceDetail": @"get_units_detailsResult"
    };
}

///--------------------------------------
#pragma mark - Transformer
///--------------------------------------

+ (NSValueTransformer *)deviceListJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:DeviceListModel.class];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return YES;
}

@end

@implementation DeviceListModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"errorCode": @"ErrorCode",
            @"errorMessage": @"ErrorMessage",
            @"transactionId": @"TransactionId",
            @"deviceList": @"UnitsList",
            @"totalDevices": @"TotalUnits"
    };
}

///--------------------------------------
#pragma mark - Transformer
///--------------------------------------

+ (NSValueTransformer *)deviceListJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:DeviceModel.class];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.errorCode == 0;
}

@end

@implementation DeviceModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"deviceId": @"UnitId",
            @"fleetId": @"subfleetid",
            @"deviceType": @"UnitType",
            @"avatar": @"Avatar",
            @"batteryLevel": @"BatteryLevel",
            @"firstReport": @"FirstReport",
            @"lastReport": @"LastReport",
            @"lastReportUtc": @"LastReportUTC",
            @"heading": @"Heading",
            @"imei": @"IMEI",
            @"inCommunication": @"In_Communication",
            @"isInstalled": @"IsInstalled",
            @"latitude": @"Latitude",
            @"longitude": @"Longitude",
            @"location": @"Location",
            @"name": @"Name",
            @"paidUntil": @"PaidUntil",
            @"rowNumber": @"RowNumber",
            @"simStatus": @"SIMStatus",
            @"serialNumber": @"SerialNumber",
            @"speed": @"Speed",
            @"status": @"Status",
            @"stockNumber": @"StockNumber",
            @"supportedFeature": @"SupportedFeature",
            @"vin": @"VIN",
            @"exteriorColor": @"VehColor",
            @"model": @"VehModel",
            @"make": @"VehMake",
            @"year": @"VehYear"
    };
}

///--------------------------------------
#pragma mark - Transformer
///--------------------------------------

+ (NSValueTransformer *)firstReportJSONTransformer
{
    return [self dotnetDateTransformer];
}

+ (NSValueTransformer *)lastReportJSONTransformer
{
    return [self dotnetDateTransformer];
}

+ (NSValueTransformer *)lastReportUtcJSONTransformer
{
    return [self dotnetDateTransformer];
}

+ (NSValueTransformer *)paidUntilJSONTransformer
{
    return [self dotnetDateTransformer];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.deviceId > 0;
}

@end
