
#import "FleetModel.h"

@implementation FleetListModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"errorCode": @"ErrorCode",
            @"errorMessage": @"ErrorMessage",
            @"transactionId": @"TransactionId",
            @"fleetList": @"SubfleetList",
            @"totalFleets": @"TotalSubfleets"
    };
}

///--------------------------------------
#pragma mark - Transformer
///--------------------------------------

+ (NSValueTransformer *)fleetListJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:FleetModel.class];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.errorCode == 0;
}

@end

@implementation FleetModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"fleetId": @"ID",
            @"name": @"Name",
            @"totalUnits": @"TotalUnits"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.fleetId > 0;
}

@end
