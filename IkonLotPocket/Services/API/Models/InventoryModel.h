
#import "ObjectModel.h"
#import "FinancingOptionModel.h"
#import "MediaModel.h"
#import "ExtraModel.h"
#import "DealerModel.h"
#import "BuildModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryModel : ObjectModel

@property (nullable, nonatomic, copy) NSString *inventoryId;
@property (nullable, nonatomic, copy) NSString *vin;
@property (nullable, nonatomic, copy) NSString *heading;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) NSInteger miles;
@property (nonatomic, assign) NSInteger msrp;
@property (nullable, nonatomic, copy) NSString *dataSource;
@property (nonatomic, assign) NSInteger isCertified;
@property (nullable, nonatomic, copy) NSString *vdpUrl;
@property (nonatomic, assign) BOOL carfaxOneOwner;
@property (nonatomic, assign) BOOL carfaxCleanTitle;
@property (nullable, nonatomic, copy) NSString *exteriorColor;
@property (nullable, nonatomic, copy) NSString *interiorColor;
@property (nonatomic, assign) NSInteger daysOnMarket;
@property (nonatomic, assign) NSInteger last180DaysOnMarket;
@property (nonatomic, assign) NSInteger daysOnMarketActive;
@property (nullable, nonatomic, copy) NSString *sellerType;
@property (nullable, nonatomic, copy) NSString *inventoryType;
@property (nullable, nonatomic, copy) NSString *stockNumber;
@property (nonatomic, assign) NSTimeInterval lastSeenAt;
@property (nullable, nonatomic, copy) NSDate *lastSeenAtDate;
@property (nonatomic, assign) NSTimeInterval scrapedAt;
@property (nullable, nonatomic, copy) NSDate *scrapedAtDate;
@property (nonatomic, assign) NSTimeInterval firstSeenAt;
@property (nullable, nonatomic, copy) NSDate *firstSeenAtDate;
@property (nonatomic, assign) NSInteger referencePrice;
@property (nonatomic, assign) NSTimeInterval referencePriceDt;
@property (nonatomic, assign) NSInteger referenceMiles;
@property (nonatomic, assign) NSTimeInterval referenceMilesDt;
@property (nullable, nonatomic, copy) NSString *source;
@property (nullable, nonatomic, copy) NSArray *financingOptions;
@property (nullable, nonatomic, copy) MediaModel *media;
@property (nullable, nonatomic, copy) ExtraModel *extra;
@property (nullable, nonatomic, copy) InventoryDealerModel *dealer;
@property (nullable, nonatomic, copy) BuildModel *build;

- (nonnull NSString *)getInventoryTitle;
- (nonnull NSString *)getDetailedInventoryTitle;
- (nonnull NSString *)getConditionStatus;

@end

NS_ASSUME_NONNULL_END
