
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define PERSISTENCECONTAINER [PersistenceContainer sharedContainer]

@class PersistenceService;

@interface PersistenceContainer : NSObject

+ (nonnull instancetype)sharedContainer;
+ (nonnull PersistenceService *)getService:(nullable Class)clazz;
- (void)setService:(nonnull Class)clazz service:(nonnull PersistenceService *)service;
- (nonnull PersistenceService *)getService:(nullable Class)clazz;

@end

NS_ASSUME_NONNULL_END