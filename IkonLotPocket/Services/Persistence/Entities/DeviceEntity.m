
#import "DeviceEntity.h"
#import "InventoryEntity.h"

@implementation DeviceEntity

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSPredicate *)deviceIdPredicate:(NSInteger)deviceId
{
    return [NSPredicate predicateWithFormat:@"deviceId == %ld", (long)deviceId];
}

+ (NSPredicate *)fleetIdPredicate:(NSInteger)fleetId
{
    return [NSPredicate predicateWithFormat:@"fleetId == %ld", (long)fleetId];
}

+ (NSPredicate *)fleetIdPredicate:(NSInteger)fleetId inventoryType:(NSString *)inventoryType
{
    if ([inventoryType isEqualToString:@"used"]) {
        return [NSPredicate predicateWithFormat:@"fleetId == %ld && vehicle.inventoryType == %@", (long)fleetId, inventoryType];
    } else {
        return [NSPredicate predicateWithFormat:@"fleetId == %ld && vehicle.inventoryType != %@", (long)fleetId, @"used"];
    }
}

+ (NSPredicate *)deviceTypePredicate:(NSInteger)deviceType
{
    return [NSPredicate predicateWithFormat:@"deviceType == %ld", (long)deviceType];
}

+ (NSPredicate *)imeiPredicate:(NSString *)imei
{
    return [NSPredicate predicateWithFormat:@"imei == %@", imei];
}
+ (NSPredicate *)serialNumberPredicate:(NSString *)serialNumber
{
    return [NSPredicate predicateWithFormat:@"serialNumber == %@", serialNumber];
}

+ (NSPredicate *)vinPredicate:(NSString *)vin
{
    return [NSPredicate predicateWithFormat:@"vin == %@", vin];
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithObjectModel:(DeviceModel *)deviceModel
{
    if ((self = [super initWithObjectModel:deviceModel])) {
        self.deviceId = deviceModel.deviceId;
        [self updateFromObjectModel:deviceModel];
    }
    return self;
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (void)updateFromObjectModel:(DeviceModel *)deviceModel
{
    self.fleetId = deviceModel.fleetId;
    self.deviceType = deviceModel.deviceType;
    self.avatar = deviceModel.avatar;
    self.batteryLevel = deviceModel.batteryLevel;
    self.firstReport = deviceModel.firstReport;
    self.lastReport = deviceModel.lastReport;
    self.lastReportUtc = deviceModel.lastReportUtc;
    self.heading = deviceModel.heading;
    self.imei = deviceModel.imei;
    self.inCommunication = deviceModel.inCommunication;
    self.isInstalled = deviceModel.isInstalled;
    self.latitude = deviceModel.latitude;
    self.longitude = deviceModel.longitude;
    self.location = deviceModel.location;
    self.name = deviceModel.name;
    self.paidUntil = deviceModel.paidUntil;
    self.rowNumber = deviceModel.rowNumber;
    self.simStatus = deviceModel.simStatus;
    self.serialNumber = deviceModel.serialNumber;
    self.speed = deviceModel.speed;
    self.status = deviceModel.status;
    self.stockNumber = deviceModel.stockNumber;
    self.vin = deviceModel.vin;
    self.exteriorColor = deviceModel.exteriorColor;
    self.model = deviceModel.model;
    self.make = deviceModel.make;
    self.year = deviceModel.year;
}

- (NSString *)getVehicleInventoryTitle
{
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@", self.year, self.make, self.model];
    return title;
}

- (BOOL)matchWithSearchText:(NSString *)searchText
{
    if ([self.description rangeOfString:searchText options:NSCaseInsensitiveSearch].length > 0) {
        return YES;
    } else {
        return NO;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@",
            self.year, self.make, self.model, self.vin, self.stockNumber, self.serialNumber, self.imei];
}

@end
