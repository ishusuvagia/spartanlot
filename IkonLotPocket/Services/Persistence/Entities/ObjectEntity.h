
#import <Realm/Realm.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ObjectEntity : RLMObject

@property (nullable, nonatomic, strong) NSString *uniqueId;

+ (nullable ObjectEntity *)objectInRealm:(nonnull RLMRealm *)realm withPredicate:(nullable NSPredicate *)predicate;
- (instancetype)initWithObjectModel:(nullable id)objectModel;
- (void)updateFromObjectModel:(nullable id)objectModel;
- (nonnull NSDictionary *)toDictionary;


@end

NS_ASSUME_NONNULL_END
