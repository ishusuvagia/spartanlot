
#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface RealmInteger : RLMObject

@property (nonatomic, assign) NSInteger integerValue;

@end

RLM_ARRAY_TYPE(RealmInteger)

@interface RealmFloat : RLMObject

@property (nonatomic, assign) CGFloat floatValue;

@end

RLM_ARRAY_TYPE(RealmFloat)

@interface RealmString : RLMObject

@property (nonatomic, strong) NSString *stringValue;

@end

RLM_ARRAY_TYPE(RealmString)

NS_ASSUME_NONNULL_END
