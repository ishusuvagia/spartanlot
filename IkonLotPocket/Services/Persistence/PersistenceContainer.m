
#import "PersistenceContainer.h"

@interface PersistenceContainer ()

@property (nonatomic, strong) NSMutableDictionary *_persistenceDict;

@end

@implementation PersistenceContainer

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)sharedContainer
{
    static PersistenceContainer *container;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        container = [PersistenceContainer new];
    });
    return container;
}

+ (PersistenceService *)getService:(Class)clazz
{
    PersistenceService *service = nil;
    @try {
        service = [PERSISTENCECONTAINER getService:clazz];
        if (!service) {
            service = (id) [[[clazz class] alloc] init];
            [PERSISTENCECONTAINER setService:clazz service:service];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception at %s, reason %@", __func__, [exception reason]);
    }
    return service;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
        __persistenceDict = [NSMutableDictionary dictionary];
    }
    return self;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setService:(Class)clazz service:(PersistenceService *)repository
{
    NSString *key = NSStringFromClass(clazz);
    PersistenceService *service = self._persistenceDict[key];
    if (service) {
        fTRACE("Request duplicate instance of repository for %@", key);
    }
    self._persistenceDict[key] = repository;
}

- (PersistenceService *)getService:(Class)clazz
{
    NSString *key = NSStringFromClass(clazz);
    PersistenceService *service = self._persistenceDict[key];
    return service;
}

@end
