
#import "DeviceService.h"
#import "InventoryService.h"

@implementation DeviceService

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_removeDevicesByFleetId:(NSInteger)fleetId
{
    NSPredicate *predicate = [DeviceEntity fleetIdPredicate:fleetId];
    RLMResults *results = [DeviceEntity objectsInRealm:self.localDb withPredicate:predicate];

    for (DeviceEntity *result in results) {
        [self.localDb deleteObject:result];
    }
}

- (RLMResults *)_getDeviceResultsByFleetId:(NSInteger)fleetId condition:(NSString *)condition sortType:(NSString *)sortType
{
    NSPredicate *predicate = [DeviceEntity fleetIdPredicate:fleetId inventoryType:condition];
    RLMResults *results = [DeviceEntity objectsInRealm:self.localDb withPredicate:predicate];
    
    if ([sortType isEqualToString:kExternal_SortBy_Model]) {
        results = [results sortedResultsUsingKeyPath:@"vehicle.model" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_HighestPrice]) {
        results = [results sortedResultsUsingKeyPath:@"vehicle.msrp" ascending:NO];
    } else if ([sortType isEqualToString:kExternal_SortBy_LowestPrice]) {
        results = [results sortedResultsUsingKeyPath:@"vehicle.msrp" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_NewestYear]) {
        results = [results sortedResultsUsingKeyPath:@"vehicle.build.year" ascending:NO];
    } else if ([sortType isEqualToString:kExternal_SortBy_OldestYear]) {
        results = [results sortedResultsUsingKeyPath:@"vehicle.year" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_LowestMileage]) {
        results = [results sortedResultsUsingKeyPath:@"vehicle.miles" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_HighestMileage]) {
        results = [results sortedResultsUsingKeyPath:@"vehicle.miles" ascending:NO];
    }
    
    return results;
}

- (void)_processInventoryByFleetId:(NSInteger)fleetId devices:(NSArray *)devices
                         condition:(NSString *)condition sortType:(NSString *)sortType
                             start:(NSInteger)start pageSize:(NSInteger)pageSize
                         responder:(id <RestResponseDelegate>)responder
{
    TRACE("fleetId=%ld devices_count=%ld start=%ld pageSize=%ld", (long)fleetId, (long)devices.count, (long)start, (long)pageSize);
    
    NSMutableArray *vins = [NSMutableArray array];
    __block NSInteger currentIdx = start + pageSize;
    NSInteger count = currentIdx < devices.count ? currentIdx : devices.count;
    for (NSInteger idx = start; idx < count; idx++) {
        DeviceModel *model = devices[idx];
        if (model.vin) [vins addObject:model.vin];
    }
    
    InventoryService *service = GET_SERVICE(InventoryService);
    [service getInventoryListByVins:vins callback:^(NSArray *inventoryList, NSInteger total, NSError *error) {
        [self.localDb beginWriteTransaction];
        for (DeviceModel *model in devices) {
            NSPredicate *predicate = [DeviceEntity deviceIdPredicate:model.deviceId];
            DeviceEntity *entity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:predicate];
            if (entity) [entity updateFromObjectModel:model];
            else entity = [[DeviceEntity alloc] initWithObjectModel:model];
            for (InventoryEntity *inventoryEntity in inventoryList) {
                if ([entity.vin caseInsensitiveCompare:inventoryEntity.vin] == NSOrderedSame) {
                    entity.vehicle = inventoryEntity;
                    break;
                }
            }
            [self.localDb addOrUpdateObject:entity];
        }
        [self.localDb commitWriteTransaction];
        if (currentIdx < devices.count) {
            [self _processInventoryByFleetId:fleetId devices:devices condition:condition sortType:sortType start:currentIdx pageSize:pageSize responder:responder];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.localDb refresh];
                RLMResults *results = [self _getDeviceResultsByFleetId:fleetId condition:condition sortType:sortType];
                if ([responder respondsToSelector:@selector(responseHandler:)]) {
                    [responder responseHandler:results];
                }
            });
        }
    }];
}

- (void)_processInventoryByFleetId:(NSInteger)fleetId devices:(NSArray *)devices start:(NSInteger)start pageSize:(NSInteger)pageSize
                          callback:(IdPagedResultBlock)callback
{
    TRACE("fleetId=%ld devices_count=%ld start=%ld pageSize=%ld", (long)fleetId, (long)devices.count, (long)start, (long)pageSize);
    
    NSMutableArray *vins = [NSMutableArray array];
    __block NSInteger currentIdx = start + pageSize;
    NSInteger count = currentIdx < devices.count ? currentIdx : devices.count;
    for (NSInteger idx = start; idx < count; idx++) {
        DeviceModel *model = devices[idx];
        if (model.vin) [vins addObject:model.vin];
    }

    InventoryService *service = GET_SERVICE(InventoryService);
    [service getInventoryListByVins:vins callback:^(NSArray *inventoryList, NSInteger total, NSError *error) {
        [self.localDb beginWriteTransaction];
        for (DeviceModel *model in devices) {
            NSPredicate *predicate = [DeviceEntity deviceIdPredicate:model.deviceId];
            DeviceEntity *entity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:predicate];
            if (entity) [entity updateFromObjectModel:model];
            else entity = [[DeviceEntity alloc] initWithObjectModel:model];
            for (InventoryEntity *inventoryEntity in inventoryList) {
                if ([entity.vin caseInsensitiveCompare:inventoryEntity.vin] == NSOrderedSame) {
                    entity.vehicle = inventoryEntity;
                    break;
                }
            }
            [self.localDb addOrUpdateObject:entity];
        }
        [self.localDb commitWriteTransaction];
        if (currentIdx < devices.count) {
            [self _processInventoryByFleetId:fleetId devices:devices start:currentIdx pageSize:pageSize callback:callback];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.localDb refresh];
                NSPredicate *fleetByIdPredicate = [DeviceEntity fleetIdPredicate:fleetId];
                RLMResults *results = [DeviceEntity objectsInRealm:self.localDb withPredicate:fleetByIdPredicate];
                NSMutableArray *pagedResults = [self pagedResultsForResults:results page:results.count pageSize:pageSize];
                if (callback) {
                    callback(pagedResults, results.count, nil);
                }
            });
        }
    }];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)buildDeviceInventoryByFleetId:(NSInteger)fleetId fleetSize:(NSInteger)fleetSize callback:(IdPagedResultBlock)callback
{
    [DataService getDeviceListByFleetId:fleetId page:0 pageSize:fleetSize callback:^(DeviceListModel *deviceListModel, NSError *error) {
        if (deviceListModel && deviceListModel.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self _processInventoryByFleetId:fleetId devices:deviceListModel.deviceList start:0 pageSize:kDefaultPageSize callback:callback];
                    } @catch (NSException *exception) {
                        [self handleException:exception pagedCallback:callback];
                    }
                }
            });
        } else {
            if (callback) callback(nil, 0, error);
        }
    }];
}

- (void)getDeviceListByFleetId:(NSInteger)fleetId totalFleetUnits:(NSInteger)totalFleetUnits
                     condition:(NSString *)condition sortType:(NSString *)sortType
                     isRefreshing:(BOOL)isRefreshing responder:(id <RestResponseDelegate>)responder
{
    if (!isRefreshing) {
        RLMResults *results = [self _getDeviceResultsByFleetId:fleetId condition:condition sortType:sortType];
        if (results.count > 0 && [responder respondsToSelector:@selector(responseHandler:)]) {
            [responder responseHandler:results];
        }
    }
    
    [DataService getDeviceListByFleetId:fleetId page:0 pageSize:totalFleetUnits callback:^(DeviceListModel *deviceListModel, NSError *error) {
        if (deviceListModel && deviceListModel.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self _processInventoryByFleetId:fleetId devices:deviceListModel.deviceList
                                               condition:condition sortType:sortType
                                                   start:0 pageSize:kDefaultPageSize responder:responder];
                    } @catch (NSException *exception) {
                        [self handleException:exception responder:responder];
                    }
                }
            });
        } else {
            if ([responder respondsToSelector:@selector(errorHandler:)]) {
                [responder errorHandler:error];
            }
        }
    }];
}

- (void)getDeviceBySerialNumber:(NSString *)serialNumber isRefreshing:(BOOL)isRefreshing responder:(id <RestResponseDelegate>)responder
{
    NSPredicate *serialNumberPredicate = [DeviceEntity serialNumberPredicate:serialNumber];

    if (!isRefreshing) {
        DeviceEntity *entity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:serialNumberPredicate];
        if (entity && [responder respondsToSelector:@selector(responseHandler:)]) {
            [responder responseHandler:entity];
        }
    }

    [DataService getDeviceBySerialNumber:serialNumber callback:^(DeviceDetailModel *model, NSError *error) {
        if (model && model.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        if (model.deviceDetail && model.deviceDetail.deviceList.count == 1) {
                            DeviceModel *deviceModel = model.deviceDetail.deviceList[0];
                            InventoryService *service = GET_SERVICE(InventoryService);
                            [service getInventoryByVin:deviceModel.vin callback:^(InventoryEntity *inventoryEntity, NSError *error) {
                                [self.localDb beginWriteTransaction];
                                NSPredicate *predicate = [DeviceEntity deviceIdPredicate:deviceModel.deviceId];
                                DeviceEntity *entity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:predicate];
                                if (entity) [entity updateFromObjectModel:deviceModel];
                                else entity = [[DeviceEntity alloc] initWithObjectModel:deviceModel];
                                entity.vehicle = inventoryEntity;
                                [self.localDb commitWriteTransaction];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.localDb refresh];
                                    DeviceEntity *deviceEntity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:serialNumberPredicate];
                                    if (deviceEntity && [responder respondsToSelector:@selector(responseHandler:)]) {
                                        [responder responseHandler:deviceEntity];
                                    }
                                });
                            }];
                        } else {
                            [self.localDb beginWriteTransaction];
                            DeviceEntity *entity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:serialNumberPredicate];
                            if (entity) [self.localDb deleteObject:entity];
                            [self.localDb commitWriteTransaction];
                        }
                    } @catch (NSException *exception) {
                        [self handleException:exception responder:responder];
                    }
                }
            });
        } else {
            if ([responder respondsToSelector:@selector(errorHandler:)]) {
                [responder errorHandler:error];
            }
        }
    }];
}

- (void)getDeviceBySerialNumber:(NSString *)serialNumber callback:(IdResultBlock)callback
{
    NSPredicate *serialNumberPredicate = [DeviceEntity serialNumberPredicate:serialNumber];

    [DataService getDeviceBySerialNumber:serialNumber callback:^(DeviceDetailModel *model, NSError *error) {
        if (model && model.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        if (model.deviceDetail && model.deviceDetail.deviceList.count == 1) {
                            DeviceModel *deviceModel = model.deviceDetail.deviceList[0];
                            InventoryService *service = GET_SERVICE(InventoryService);
                            [service getInventoryByVin:deviceModel.vin callback:^(InventoryEntity *inventoryEntity, NSError *error) {
                                [self.localDb beginWriteTransaction];
                                NSPredicate *predicate = [DeviceEntity deviceIdPredicate:deviceModel.deviceId];
                                DeviceEntity *entity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:predicate];
                                if (entity) [entity updateFromObjectModel:deviceModel];
                                else entity = [[DeviceEntity alloc] initWithObjectModel:deviceModel];
                                entity.vehicle = inventoryEntity;
                                [self.localDb commitWriteTransaction];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.localDb refresh];
                                    DeviceEntity *deviceEntity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:serialNumberPredicate];
                                    if (callback) {
                                        callback(deviceEntity, nil);
                                    }
                                });
                            }];
                        } else {
                            [self.localDb beginWriteTransaction];
                            DeviceEntity *entity = (DeviceEntity *) [DeviceEntity objectInRealm:self.localDb withPredicate:serialNumberPredicate];
                            if (entity) [self.localDb deleteObject:entity];
                            [self.localDb commitWriteTransaction];
                        }

                    } @catch (NSException *exception) {
                        [self handleException:exception callback:callback];
                    }
                }
            });
        } else {
            if (callback) callback(nil, error);
        }
    }];
}

- (void)searchDevice:(NSString *)searchText fleetId:(NSInteger)fleetId condition:(NSString *)condition responder:(id <RestResponseDelegate>)responder
{
    NSPredicate *predicate = [DeviceEntity fleetIdPredicate:fleetId inventoryType:condition];
    
    RLMResults *results = [DeviceEntity objectsInRealm:self.localDb withPredicate:predicate];
    NSMutableArray *searchResult = [NSMutableArray array];
    
    if (![Utils stringIsNullOrEmpty:searchText]) {
        for (DeviceEntity *deviceEntity in results) {
            if ([deviceEntity matchWithSearchText:searchText]) {
                [searchResult addObject:deviceEntity];
            }
        }
    } else {
        for (DeviceEntity *deviceEntity in results) {
            [searchResult addObject:deviceEntity];
        }
    }
    
    if ([responder respondsToSelector:@selector(responseHandler:)]) {
        [responder responseHandler:searchResult];
    }
}

@end
