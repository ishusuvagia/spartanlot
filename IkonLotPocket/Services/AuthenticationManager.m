
#import "AuthenticationManager.h"
#import "ModulesManager.h"
#import "DataService.h"
#import "DeviceService.h"

@implementation AuthenticationManager

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)sharedManager;
{
    static AuthenticationManager *authManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        authManager = [AuthenticationManager new];
    });
    return authManager;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)loginWithUsername:(NSString *)username password:(NSString *)password callback:(BooleanResultBlock)callback
{
    [DataService loginWithUsername:username password:password callback:^(AccountModel *model, NSError *error) {
        if (model && model.isValid && !error) {
            self->_isAuthenticated = YES;
            self->_userId = model.userId;
            self->_token = model.token;
            self->_name = model.name;
            self->_companyId = model.companyId;
            self->_transactionId = model.transactionId;
            [Utils saveObjectToUserDefaults:username forKey:kPrefUsernameKey];
            [Utils saveObjectToUserDefaults:password forKey:kPrefPasswordKey];
            [MODULESMANAGER initModules:nil];
            [DataService getFleetList:self.token companyId:self.companyId callback:^(FleetListModel *fleetListModel, NSError *error) {
                if (fleetListModel.fleetList.count > 0) {
                    FleetModel *fleetModel = [fleetListModel.fleetList firstObject];
                    self->_fleetId = fleetModel.fleetId;
                    self->_totalFleetUnits = fleetModel.totalUnits;
                    self->_currentDealerName = fleetModel.name;
                }
                if (callback) callback(YES, nil);
//                DeviceService *service = GET_SERVICE(DeviceService);
//                [service buildDeviceInventoryByFleetId:self.fleetId fleetSize:self.totalFleetUnits callback:nil];
            }];
        } else {
            self->_isAuthenticated = NO;
            if (callback) callback(NO, error);
        }
    }];
}

- (void)logout
{
    _userId = 0;
    _token = nil;
    _name = nil;
    _companyId = 0;
    _transactionId = 0;
    [Utils saveObjectToUserDefaults:nil forKey:kPrefPasswordKey];
}

///--------------------------------------
#pragma mark - Class
///--------------------------------------

@end
