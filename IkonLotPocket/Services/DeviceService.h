
#import "PersistenceService.h"
#import "DeviceEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeviceService : PersistenceService

- (void)buildDeviceInventoryByFleetId:(NSInteger)fleetId fleetSize:(NSInteger)fleetSize callback:(nullable IdPagedResultBlock)callback;
- (void)getDeviceListByFleetId:(NSInteger)fleetId totalFleetUnits:(NSInteger)totalFleetUnits
                     condition:(nullable NSString *)condition sortType:(nullable NSString *)sortType
                  isRefreshing:(BOOL)isRefreshing responder:(nullable id <RestResponseDelegate>)responder;
- (void)getDeviceBySerialNumber:(nullable NSString *)serialNumber isRefreshing:(BOOL)isRefreshing responder:(nullable id<RestResponseDelegate>)responder;
- (void)getDeviceBySerialNumber:(nullable NSString *)serialNumber callback:(nullable IdResultBlock)callback;
- (void)searchDevice:(nullable NSString *)searchText fleetId:(NSInteger)fleetId condition:(nullable NSString *)condition
           responder:(nullable id <RestResponseDelegate>)responder;

@end

NS_ASSUME_NONNULL_END
