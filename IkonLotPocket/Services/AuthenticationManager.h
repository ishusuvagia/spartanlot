
#import <Foundation/Foundation.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

#define AUTHMANAGER [AuthenticationManager sharedManager]

@interface AuthenticationManager : NSObject

@property (nonatomic, assign, readonly) NSInteger userId;
@property (nullable, nonatomic, readonly) NSString *token;
@property (nullable, nonatomic, readonly) NSString *name;
@property (nonatomic, assign, readonly) NSInteger companyId;
@property (nonatomic, assign, readonly) NSInteger transactionId;
@property (nonatomic, assign, readonly) NSInteger fleetId;
@property (nonatomic, assign, readonly) NSInteger totalFleetUnits;
@property (nullable, nonatomic, readonly) NSString *currentDealerId;
@property (nullable, nonatomic, readonly) NSString *currentDealerName;
@property (nonatomic, assign, readonly) BOOL isAuthenticated;

+ (nonnull instancetype)sharedManager;
- (void)loginWithUsername:(nullable NSString *)username password:(nullable NSString *)password callback:(nullable BooleanResultBlock)callback;
- (void)logout;

@end

NS_ASSUME_NONNULL_END
