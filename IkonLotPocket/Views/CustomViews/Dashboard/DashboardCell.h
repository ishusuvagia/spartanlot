
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardCellView : UIView

@property (nullable, nonatomic, strong) UIImage *image;
@property (nullable, nonatomic, strong) NSString *text;
@property (nonatomic, assign) NSInteger badgeCount;

@end

@interface DashboardCell : UICollectionViewCell

@property (nullable, nonatomic, strong) DashboardCellView *content;

@end

NS_ASSUME_NONNULL_END
