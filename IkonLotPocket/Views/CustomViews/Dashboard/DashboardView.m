
#import "DashboardView.h"
#import "DashboardCell.h"
#import "ModulesManager.h"
#import "NavigationController.h"
#import "ScrollableTabbarViewController.h"

static const NSInteger kColumnsCount = 2;
static const NSInteger kRowsCount = 4;
static NSString *kCellIdentifier = @"dashboardcell";

@interface DashboardModule : NSObject

@property (nonatomic, weak) id <ModuleDelegate> _delegate;
@property (nonatomic, strong) DashboardCellView *_button;

- (instancetype)initWithModule:(id <ModuleDelegate>)module;

@end

@implementation DashboardModule

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithModule:(id <ModuleDelegate>)module
{
    if ((self = [super init])) {
        __delegate = module;
        __button = [DashboardCellView new];
        __button.text = [module getTitle];
        __button.image = [self _preprocessImage:[module getDashboardIcon]];
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (BOOL)_isAllowed
{
    return YES;
}

- (UIImage *)_preprocessImage:(UIImage *)image
{
    if (![self _isAllowed]) {
        return [self _preprocessImageNotAllowed:image];
    }
    return image;
}

- (UIImage *)_preprocessImageNotAllowed:(UIImage *)image
{
    CGFloat alpha = 0.6f;
    UIGraphicsBeginImageContextWithOptions(image.size, false, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0.0f, 0.0f, image.size.width, image.size.height);
    CGContextScaleCTM(context, 1.0f, -1.0f);
    CGContextTranslateCTM(context, 0, -area.size.height);
    CGContextSetBlendMode(context, kCGBlendModeMultiply);
    CGContextSetAlpha(context, alpha);
    CGContextDrawImage(context, area, image.CGImage);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end

@interface DashboardView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *_items;
@property (nonatomic, strong) NSTimer *_badgeRefreshTimer;
@property (nonatomic, strong) UIViewController *_parentViewController;

@end

@implementation DashboardView

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self registerClass:[DashboardCell class] forCellWithReuseIdentifier:kCellIdentifier];
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSInteger)_totalRowsCount
{
    NSInteger numberOfRows = (NSInteger) ceil(self._items.count / (CGFloat)kColumnsCount);
    NSInteger totalRowsCount = MAX(kRowsCount, numberOfRows);
    // fTRACE("totalRowsCount=%ld", (long) totalRowsCount);
    return totalRowsCount;
}

- (NSInteger)_visibleRowsCount
{
    NSInteger visibleRowsCount = MIN([self _totalRowsCount], kRowsCount);
    // fTRACE("visibleRowsCount=%ld", (long) visibleRowsCount);
    return visibleRowsCount;
}

- (CGSize)_calculateButtonSize
{
    CGSize size = self.frame.size;
    CGFloat buttonWidth = size.width / kColumnsCount;
    CGFloat buttonHeight = size.height / [self _visibleRowsCount];
    // fTRACE("buttonWidth=%.2f buttonHeight=%.2f", buttonWidth, buttonHeight);
    return CGSizeMake(buttonWidth, buttonHeight);
}

- (void)_showModule:(DashboardModule *)module
{
    if ([module _isAllowed]) {
        ModuleType type = [module._delegate getModuleType];
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [navController hidesBarsOnTap];
        [self._parentViewController.navigationController presentViewController:navController animated:YES completion:nil];
        
    }
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)initButtons:(UIViewController *)parentViewController
{
    __parentViewController = parentViewController;

    NSMutableArray *visibleModules = [NSMutableArray new];
    NSArray *allowedModules = [MODULESMANAGER getAllowedModules];
    for (id <ModuleDelegate> module in allowedModules) {
        DashboardModule *dashboardModule = [[DashboardModule alloc] initWithModule:module];
        if ([dashboardModule _isAllowed]) {
            [visibleModules addObject:dashboardModule];
        }
    }

    __items = visibleModules;
    [self reloadData];

    BOOL scrollable = [self _totalRowsCount] > kRowsCount;
    self.scrollEnabled = scrollable;
    self.bounces = scrollable;
}

///--------------------------------------
#pragma mark - <UICollectionViewDataSource>
///--------------------------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self._items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DashboardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    DashboardModule *module = self._items[(NSUInteger) indexPath.row];
    cell.content = module._button;
    return cell;
}

///--------------------------------------
#pragma mark - <UICollectionViewDelegate>
///--------------------------------------

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    DashboardModule *module = self._items[(NSUInteger) indexPath.row];
    [self _showModule:module];
}

///--------------------------------------
#pragma mark - <UICollectionViewDelegateFlowLayout>
///--------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    return [self _calculateButtonSize];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return CGSizeMake(collectionView.frame.size.width/3, 125);
        
    }
    else
    {
        NSLog(@"width %f",collectionView.frame.size.width);
        return CGSizeMake(collectionView.frame.size.width/3, 150);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}

@end

/// </editor-fold>
