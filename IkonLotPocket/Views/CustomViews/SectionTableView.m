
#import <MJRefresh/MJRefresh.h>
#import "SectionTableView.h"
#import "EmptyDataView.h"

@interface SectionTableView () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
{
    BOOL _hasHeaderSection;
    BOOL _hasFooterSection;
    CGFloat _lastContentOffset;
}

@property (nullable, nonatomic, strong) EmptyDataView *_emptyDataView;
@property (nonatomic, strong) NSMutableArray *_cellSectionData;
@property (nonatomic, strong) NSArray *_cellPrototypes;
@property (nonatomic, strong) NSIndexPath *_selectedIndexPath;
@property (nonatomic, weak) id <SectionTableViewDelegate> _delegate;
@property (nonatomic, strong) MJRefreshNormalHeader *_refreshHeader;
@property (nonatomic, strong) MJRefreshAutoNormalFooter *_refreshFooter;

@end

@implementation SectionTableView

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self _init];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self _init];
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_init
{
    __cellSectionData = [NSMutableArray array];
    self.dataSource = self;
    self.delegate = self;
}

- (void)_refresh:(id)sender
{
    if (self._emptyDataView) {
        [self._emptyDataView hide];
    }
    if ([self._delegate respondsToSelector:@selector(refreshData)]) {
        [self._delegate refreshData];
    } else {
        TRACE("Delegate=%@ must implement the refreshData method", self._delegate);
        [self._refreshHeader endRefreshing];
    }
}

- (void)_fetch:(id)sender
{
    if (self._emptyDataView) {
        [self._emptyDataView hide];
    }
    if ([self._delegate respondsToSelector:@selector(loadMoreData)]) {
        [self._delegate loadMoreData];
    } else {
        TRACE("Delegate=%@ must implement the refreshData method", self._delegate);
        [self._refreshFooter endRefreshing];
    }
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)initWithCellPrototypes:(NSArray<NSDictionary *> *)cellPrototypes
              hasHeaderSection:(BOOL)hasHeaderSection
              hasFooterSection:(BOOL)hasFooterSection
                 enableRefresh:(BOOL)enableRefresh
                   enableFetch:(BOOL)enableFetch
                 emptyDataText:(NSString *)emptyDataText
                      delegate:(id <SectionTableViewDelegate>)delegate
{
    __cellPrototypes = cellPrototypes;
    _hasHeaderSection = hasHeaderSection;
    _hasFooterSection = hasFooterSection;
    __delegate = delegate;
    
    if (!_hasFooterSection) {
        self.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    
    // Register all the prototype UITableViewCell for the current UITableView
    for (NSDictionary *prototype in self._cellPrototypes) {
        REGISTER_CELL(self, prototype[kCellNibNameKey], prototype[kCellIdentifierKey]);
    }
    
    // Setup custom views
    if (!self._emptyDataView) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        
        __emptyDataView = [[EmptyDataView alloc]
                           initWithFrame:CGRectMake(screenWidth / 2 - [EmptyDataView width] / 2,
                                                    screenHeight / 2 - [EmptyDataView height] / 2 - kNavigationBarDefaultHeight,
                                                    [EmptyDataView width], [EmptyDataView height])];
        [__emptyDataView updateViewWithText:emptyDataText ?: LOCALIZED(@"no_availabledata")];
        [self.superview addSubview:__emptyDataView];
    }
    
    if (enableRefresh) {
        if (!self._refreshHeader) {
            __refreshHeader = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(_refresh:)];
        }
        self._refreshHeader.stateLabel.hidden = YES;
        self._refreshHeader.lastUpdatedTimeLabel.hidden = YES;
        self.mj_header = self._refreshHeader;
    }
    
    if (enableFetch) {
        if (!self._refreshFooter) {
            __refreshFooter = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(_fetch:)];
        }
        [self._refreshFooter setTitle:kEmptyString forState:MJRefreshStateIdle];
        [self._refreshFooter setTitle:LOCALIZED(@"load_more_data") forState:MJRefreshStateRefreshing];
        self._refreshFooter.onlyRefreshPerDrag = NO;
        self.mj_footer = self._refreshFooter;
    }
}

- (UIView *)buildHeaderFooterView:(NSString *)text alignment:(NSTextAlignment)textAligment
{
    CGFloat width = self.bounds.size.width;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width, kHeaderFooterHeight)];
    view.backgroundColor = [AppColor tableViewHeaderBackgroundColor];
    view.userInteractionEnabled = YES;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kHeaderFooterPadding, 0,
                                                               width - kHeaderFooterPadding * 2, kHeaderFooterHeight)];
    label.backgroundColor = [AppColor tableViewSectionIndexBackgroundColor];
    label.textColor = [AppColor tableViewHeaderForegroundColor];
    label.font = [AppFont tableHeaderFooterFont];
    label.text = text;
    label.textAlignment = textAligment;
    [view addSubview:label];
    
    return view;
}

- (BOOL)isEmpty
{
    return self._cellSectionData.count == 0;
}

- (void)emptyData
{
    [self._cellSectionData removeAllObjects];
    [self reload];
}

- (void)loadData:(NSArray *)cellSectionData
{
    self._cellSectionData = [cellSectionData mutableCopy];
    [self reload];
}

- (void)reload
{
    if (self._cellSectionData.count == 0) {
        [self._emptyDataView show];
    } else {
        [self._emptyDataView hide];
    }
    [self reloadData];
}

- (BOOL)isRefreshing
{
    return (self._refreshHeader && self._refreshHeader.isRefreshing);
}

- (BOOL)isFetching
{
    return (self._refreshFooter && self._refreshFooter.isRefreshing);
}

- (void)endRefreshing
{
    if (self._refreshHeader) {
        [self._refreshHeader endRefreshing];
    }
    if (self._refreshFooter) {
        [self._refreshFooter endRefreshing];
    }
}

- (void)updateEmptyDataText:(nullable NSString *)emptyDataText
{
    if (self._emptyDataView) {
        [self._emptyDataView updateViewWithText:emptyDataText ?: LOCALIZED(@"no_availabledata")];
    }
}

- (void)hideEmptyView
{
    [self._emptyDataView hide];
}

///--------------------------------------
#pragma mark - <UITableViewDataSource>
///--------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numSection = self._cellSectionData.count;
    // TRACE("Number of section: %ld", (long)numSection);
    return numSection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionData = self._cellSectionData[section][kSectionDataKey];
    NSInteger numRows = sectionData.count;
    // TRACE("Number of rows: %ld for section: `%@`", (long)numRows, self.cellSectionData[section][kSectionTitleKey]);
    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    @try {
        NSArray *sectionData = self._cellSectionData[indexPath.section][kSectionDataKey];
        NSDictionary *cellDict = sectionData[indexPath.row];
        // TRACE("Row Index: %ld - Section Index: %ld", (long)indexPath.row, (long)indexPath.section);
        cell = REUSECELL_INIT(tableView, cellDict[kCellClassKey], cellDict[kCellIdentifierKey]);
        if ([self._delegate respondsToSelector:@selector(setupCell:withDictionary:atIndexPath:)]) {
            [self._delegate setupCell:cell withDictionary:cellDict atIndexPath:indexPath];
        }
        [cell updateConstraintsIfNeeded];
    }
    @catch (NSException *exception) {
        NSLog(@"%s: Exception: %@", __func__, exception);
    }
    
    return cell;
}

///--------------------------------------
#pragma mark - <UITableViewDelegate>
///--------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.tableViewProxyDelegate respondsToSelector:@selector(tableView:heightForHeaderInSection:)]) {
        return [self.tableViewProxyDelegate tableView:tableView heightForHeaderInSection:section];
    } else {
        if (_hasHeaderSection) {
            NSString *headerText = self._cellSectionData[section][kSectionTitleKey];
            if ([Utils stringIsNullOrEmpty:headerText])
                return 0.001f;
            return kHeaderFooterHeight;
        } else {
            return 0.001f;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([self.tableViewProxyDelegate respondsToSelector:@selector(tableView:heightForFooterInSection:)]) {
        return [self.tableViewProxyDelegate tableView:tableView heightForFooterInSection:section];
    } else {
        return 0.001f;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([self.tableViewProxyDelegate respondsToSelector:@selector(tableView:viewForHeaderInSection:)]) {
        return [self.tableViewProxyDelegate tableView:tableView viewForHeaderInSection:section];
    } else {
        NSString *headerText = self._cellSectionData[section][kSectionTitleKey];
        return [self buildHeaderFooterView:headerText alignment:NSTextAlignmentLeft];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sectionData = self._cellSectionData[indexPath.section][kSectionDataKey];
    NSDictionary *cellDict = sectionData[indexPath.row];
    CGFloat cellHeight = [cellDict[kCellHeightKey] floatValue];
    // TRACE("Cell Height: %.2f", cellHeight);
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *sectionData = self._cellSectionData[(NSUInteger) indexPath.section][kSectionDataKey];
    NSDictionary *cellDict = sectionData[(NSUInteger) indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    self._selectedIndexPath = indexPath;
    TRACE("Selected cell at sectionIndex: %ld, rowIndex: %ld", (long) indexPath.section, (long) indexPath.row);
    if ([self._delegate respondsToSelector:@selector(didSelectCell:sectionTitle:withDictionary:atIndexPath:)]) {
        [self._delegate didSelectCell:cell sectionTitle:self._cellSectionData[(NSUInteger) indexPath.section][kSectionTitleKey]
                       withDictionary:cellDict atIndexPath:indexPath];
    }
}

///--------------------------------------
#pragma mark - <UIScrollViewDelegate>
///--------------------------------------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
        CGFloat offset = 0.0f;
    
        ScrollDirection scrollDirection = ScrollDirectionNone;
        if (_lastContentOffset > scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionDown;
        else if (_lastContentOffset < scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionUp;
        offset = scrollView.contentOffset.y;
    
        CGFloat offsetDiff = (CGFloat) fabs(offset - _lastContentOffset);
        if (scrollDirection == ScrollDirectionDown && offsetDiff >= 5.0f) {
            [self endEditing:YES];
        }
        _lastContentOffset = offset;
}

@end
