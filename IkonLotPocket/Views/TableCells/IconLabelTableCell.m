
#import "IconLabelTableCell.h"

@interface IconLabelTableCell ()

@property (weak, nonatomic) IBOutlet UIImageView *_iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;

@property (nonatomic, strong) NSString *_key;

@end

@implementation IconLabelTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 56.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title iconImage:(UIImage *)iconImage key:(NSString *)key showDisclosure:(BOOL)showDisclosure
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    self._iconImgView.image = iconImage;
    if (showDisclosure) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    self._key = key;
}

- (void)setupCellWithTitle:(NSString *)title iconImage:(UIImage *)iconImage colorCode:(UIColor *)colorCode
                       key:(NSString *)key showDisclosure:(BOOL)showDisclosure
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    self._titleLbl.textColor = colorCode;
    self._iconImgView.image = iconImage;
    self._iconImgView.tintColor = colorCode;
    if (showDisclosure) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    self._key = key;
}


@end
