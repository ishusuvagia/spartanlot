
#import "RoundTableCell.h"
#import "AppColorScheme.h"
#import "CheckMark.h"
#import "UITools.h"
#import "Utils.h"

@interface RoundTableCell ()

@property (weak, nonatomic) IBOutlet UIView *_containerView;
@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;
@property (weak, nonatomic) IBOutlet UIView *_colorCodeIndicatorView;

@end

@implementation RoundTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 100.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentView.backgroundColor = [AppColorScheme clear];
    self.backgroundColor = [AppColorScheme clear];
    self._containerView.backgroundColor = [AppColorScheme brandWhite];
    [UITools roundView:self._containerView cornerRadius:3.0f];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title colorCode:(UIColor *)colorCode
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? [title uppercaseString] : @"-";
    self._titleLbl.textColor = [AppColorScheme brandBlack];
    self._colorCodeIndicatorView.backgroundColor = colorCode ?: [AppColorScheme brandBlack];
}

@end
