
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfoTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithTitle:(nullable NSString *)title information:(nullable NSString *)information
               customColor:(nullable UIColor *)customColor showDisclosure:(BOOL)showDisclosure;

@end

NS_ASSUME_NONNULL_END
