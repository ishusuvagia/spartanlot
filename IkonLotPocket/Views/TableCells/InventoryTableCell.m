
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "InventoryTableCell.h"

@interface InventoryTableCell ()

@property (weak, nonatomic) IBOutlet UIImageView *_imageView;
@property (weak, nonatomic) IBOutlet UILabel *_statusLbl;
@property (weak, nonatomic) IBOutlet UIImageView *_satIconImgView;

@property (weak, nonatomic) IBOutlet UILabel *_inventoryCodeLbl;
@property (weak, nonatomic) IBOutlet UILabel *_titlelbl;
@property (weak, nonatomic) IBOutlet UILabel *_vinLbl;
@property (weak, nonatomic) IBOutlet UILabel *_priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *_mileageLbl;
@property (weak, nonatomic) IBOutlet UILabel *_exteriorTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *_interiorTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *_exteriorColorLbl;
@property (weak, nonatomic) IBOutlet UILabel *_interiorColorLbl;
@property (weak, nonatomic) IBOutlet UILabel *_inventoryCondition;

@property (weak, nonatomic) IBOutlet UIImageView *_batteryIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *_batteryLevelLbl;

@end

@implementation InventoryTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 120;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
     [self._imageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self._exteriorTitleLbl.text = [NSString stringWithFormat:@"%@:", LOCALIZED(@"exterior")];
    self._interiorTitleLbl.text = [NSString stringWithFormat:@"%@:", LOCALIZED(@"interior")];
    [UITools dropShadow:self shadowOpacity:1.0f shadowRadius:0.5f offset:CGSizeZero];
    self._imageView.layer.borderColor = [AppColorScheme lightGray].CGColor;
    self._imageView.layer.borderWidth = 0.5f;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(nullable NSString *)title condition:(nullable NSString *)condition
             inventoryCode:(nullable NSString *)inventoryCode vin:(nullable NSString *)vin
                 msrpPrice:(NSInteger)msrpPrice mileage:(NSInteger)mileage
             exteriorColor:(nullable NSString *)exteriorColor interiorColor:(nullable NSString *)interiorColor
           deviceInstalled:(BOOL)deviceInstalled
              batteryLevel:(nullable NSString *)batteryLevel
                  imageUrl:(nullable NSString *)imageUrl
                 condition:(nullable NSString *)inventoryCondition
{
    // Image
    [self._imageView sd_addActivityIndicator];
    [self._imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                       placeholderImage:[UIImage imageNamed:@"img-inventory-placeholder"]
                                options:SDWebImageRetryFailed|SDWebImageRefreshCached|SDWebImageContinueInBackground
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
    // Condition
    self._statusLbl.text = [LOCALIZED(condition) uppercaseString];
    self._statusLbl.textColor = [BusinessUtils getVehicleConditionColor:condition];

    // Inventory Code
    self._inventoryCodeLbl.text = [NSString stringWithFormat:@"%@ #: %@",LOCALIZED(@"stocknumber"),inventoryCode];//![Utils stringIsNullOrEmpty:inventoryCode] ? [inventoryCode uppercaseString] : @"-";

    // Title
    self._titlelbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";

    // Vin Number
    self._vinLbl.text = ![Utils stringIsNullOrEmpty:vin] ? [vin uppercaseString] : @"-";

    // Price
    self._priceLbl.text = [NSString stringWithFormat:@"%@ %@", [Utils getCurrencyString:msrpPrice withDecimal:NO], LOCALIZED(@"msrp")];

    // Mileage
    self._mileageLbl.text = [NSString stringWithFormat:@"%@: %@ mi", LOCALIZED(@"mileage"), [Utils getNumberString:mileage]];

    // Exterior Color
    self._exteriorColorLbl.text = ![Utils stringIsNullOrEmpty:exteriorColor] ? [Utils capitalizedOnlyFirstLetter:exteriorColor]  : @"-";

    // Interior Color
    self._interiorColorLbl.text = ![Utils stringIsNullOrEmpty:interiorColor] ? [Utils capitalizedOnlyFirstLetter:interiorColor] : @"-";
    
    // Satelite Icon
    self._satIconImgView.hidden = !deviceInstalled;
    
    self._inventoryCondition.text=[NSString stringWithFormat:@"%@",inventoryCondition];
    
    // Battery Level
    CGFloat level = [BusinessUtils getBatteryLevel:batteryLevel];
    self._batteryIconImgView.image = [BusinessUtils getBatteryIconImage:level];
    self._batteryIconImgView.tintColor = [BusinessUtils getBatteryLevelColor:level];
    self._batteryLevelLbl.text = [[BusinessUtils getBatteryLevelString:level] uppercaseString];
    self._batteryLevelLbl.textColor = [BusinessUtils getBatteryLevelColor:level];

}

@end
