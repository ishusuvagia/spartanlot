
#import "RangeSliderTableCell.h"

@interface RangeSliderTableCell () <TTRangeSliderDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *_iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;
@property (weak, nonatomic) IBOutlet TTRangeSlider *_rangeSlider;

@property (nonatomic, strong) NSString *_key;
@property (nonatomic, weak) id<RangeSliderDelegate> _delegate;

@end

@implementation RangeSliderTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 97.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    self._rangeSlider.minLabelColour = [AppColorScheme red];
    self._rangeSlider.maxLabelColour = [AppColorScheme red];
    self._rangeSlider.minLabelFont = [AppFont statusFont];
    self._rangeSlider.maxLabelFont = [AppFont statusFont];
    self._rangeSlider.labelPadding = 4.0f;
    self._rangeSlider.selectedHandleDiameterMultiplier = 1;
    self._rangeSlider.tintColorBetweenHandles = [AppColorScheme red];
    self._rangeSlider.tintColor = [AppColorScheme brandGray];
    self._rangeSlider.delegate = self;
}

///--------------------------------------
#pragma mark - <TTRangeSliderDelegate>
///--------------------------------------

-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum
{
    if ([self._delegate respondsToSelector:@selector(rangeSlider:didChangeSelectedMinimumValue:andMaximumValue:key:)]) {
        [self._delegate rangeSlider:sender didChangeSelectedMinimumValue:selectedMinimum andMaximumValue:selectedMaximum key:self._key];
    }
}

- (void)didEndTouchesInRangeSlider:(TTRangeSlider *)sender
{
    if ([self._delegate respondsToSelector:@selector(didEndTouchesInRangeSlider:)]) {
        [self._delegate didEndTouchesInRangeSlider:sender];
    }
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title iconImage:(UIImage *)iconImage
                   minimum:(CGFloat)minimum maximum:(CGFloat)maximum
           selectedMinimum:(CGFloat)selectedMinimum selectedMaximum:(CGFloat)selectedMaximum
           numberFormatter:(NSNumberFormatter *)numberFormatter
                       key:(NSString *)key delegate:(id<RangeSliderDelegate>)delegate
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    self._iconImgView.image = iconImage;
    
    self._rangeSlider.minValue = (float) minimum;
    self._rangeSlider.maxValue = (float) maximum;
    self._rangeSlider.selectedMinimum = (float) selectedMinimum;
    self._rangeSlider.selectedMaximum = (float) selectedMaximum;
    self._rangeSlider.numberFormatterOverride = numberFormatter;
    self._delegate = delegate;
    self._key = key;
}

@end
