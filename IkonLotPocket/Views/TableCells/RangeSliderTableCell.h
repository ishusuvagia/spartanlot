
#import <UIKit/UIKit.h>
#import <TTRangeSlider/TTRangeSlider.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RangeSliderDelegate <NSObject>
@optional
- (void)rangeSlider:(nullable TTRangeSlider *)sender didChangeSelectedMinimumValue:(CGFloat)selectedMinimum andMaximumValue:(CGFloat)selectedMaximum key:(nullable NSString *)key;
- (void)didEndTouchesInRangeSlider:(nullable TTRangeSlider *)sender;
- (void)didStartTouchesInRangeSlider:(nullable TTRangeSlider *)sender;
@end

@interface RangeSliderTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithTitle:(nullable NSString *)title iconImage:(nullable UIImage *)iconImage
                   minimum:(CGFloat)minimum maximum:(CGFloat)maximum
           selectedMinimum:(CGFloat)selectedMinimum selectedMaximum:(CGFloat)selectedMaximum
           numberFormatter:(NSNumberFormatter *)numberFormatter
                       key:(nullable NSString *)key delegate:(nullable id<RangeSliderDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
