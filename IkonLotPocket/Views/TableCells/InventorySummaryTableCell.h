
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InventorySummaryTableCell : UITableViewCell

+ (CGFloat)heightByImageCount:(NSInteger)imageCount;
- (void)setupCellWithImages:(nullable NSArray *)imageUrls title:(nullable NSString *)title
                      price:(NSInteger)price msrpPrice:(NSInteger)msrpPrice
                  condition:(nullable NSString *)condition;

@end

NS_ASSUME_NONNULL_END
