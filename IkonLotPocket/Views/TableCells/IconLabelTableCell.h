
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IconLabelTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithTitle:(nullable NSString *)title iconImage:(nullable UIImage *)iconImage key:(nullable NSString *)key showDisclosure:(BOOL)showDisclosure;
- (void)setupCellWithTitle:(nullable NSString *)title iconImage:(nullable UIImage *)iconImage colorCode:(nullable UIColor *)colorCode
                       key:(nullable NSString *)key showDisclosure:(BOOL)showDisclosure;

@end

NS_ASSUME_NONNULL_END
