
#import <ASHorizontalScrollViewForObjectiveC/ASHorizontalScrollView.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "OrderedDictionary.h"
#import "InventorySummaryTableCell.h"

static NSInteger const kTagOverlay = 1001;

@interface InventorySummaryTableCell ()

@property (weak, nonatomic) IBOutlet UIImageView *_selectedImgView;
@property (weak, nonatomic) IBOutlet ASHorizontalScrollView *_scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *_scrollViewHeightConstant;
@property (weak, nonatomic) IBOutlet UILabel *_titlelbl;
@property (weak, nonatomic) IBOutlet UILabel *_msrpPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *_priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *_conditionLbl;
@property (weak, nonatomic) IBOutlet UIView *_textContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *_textContainerHeightConstant;

@property (nonatomic, strong) MutableOrderedDictionary *_imageViews;
@property (nonatomic, strong) NSString *_selectedImageUrl;

@end

@implementation InventorySummaryTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)heightByImageCount:(NSInteger)imageCount
{
    CGFloat imageWidth = [UITools currentDeviceRect].size.width;
    CGFloat imageHeight = imageWidth * 17.0f / 25.0f;
    
    if (imageCount <= 1) {
        return imageHeight + 170.0f + 5.0f - 73.0f;
    } else {
        return imageHeight + 170.0f + 5.0f;
    }
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    __imageViews = [MutableOrderedDictionary dictionary];
    [self._selectedImgView sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self._scrollView.leftMarginPx = 16.0f;
    self._scrollView.miniMarginPxBetweenItems = 16.0f;
    self._scrollView.miniAppearPxOfLastItem = 16.0f;
    self._scrollView.uniformItemSize = CGSizeMake(97.0f, 73.0f);
    [self._scrollView setItemsMarginOnce];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_addOverlayToImageView:(UIImageView *)imageView
{
    UIView *overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
    overlay.tag = kTagOverlay;
    [overlay setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [imageView addSubview:overlay];
}

- (void)_removeOverlayFromImageView:(UIImageView *)imageView
{
    for (UIView *view in imageView.subviews) {
        if (view.tag == kTagOverlay) {
            [view removeFromSuperview];
            break;
        }
    }
}

- (void)_handleImageViewTapEvent:(UITapGestureRecognizer *)recognizer
{
    TRACE("Sender: %@ - View: %@", recognizer, recognizer.view);
    if ([recognizer.view isKindOfClass:[UIImageView class]]) {
        UIImageView *currentSelectedImageView = [self._imageViews objectForKey:self._selectedImageUrl];
        if (currentSelectedImageView) {
            [self _removeOverlayFromImageView:currentSelectedImageView];
        }
        UIImageView *imageView = (UIImageView *) recognizer.view;
        NSString *imageUrl = [self._imageViews keyAtIndex:imageView.tag];
        self._selectedImageUrl = imageUrl;
        self._selectedImgView.image = imageView.image;
        [self _addOverlayToImageView:imageView];
    }
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithImages:(NSArray *)imageUrls title:(NSString *)title
                      price:(NSInteger)price msrpPrice:(NSInteger)msrpPrice
                  condition:(NSString *)condition
{
    if (imageUrls.count > 0) {
        NSString *imageUrl = imageUrls[0];
        if ([Utils stringIsNullOrEmpty:self._selectedImageUrl] || (imageUrls.count != self._imageViews.count)) {
            __selectedImageUrl = imageUrl;
        }
        [self._selectedImgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                                 placeholderImage:[UIImage imageNamed:@"img-inventory-placeholder"]
                                          options:SDWebImageRetryFailed|SDWebImageRefreshCached|SDWebImageContinueInBackground
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];

        if (imageUrls.count == 1) {
            self._scrollViewHeightConstant.constant = 0.0f;
            self._textContainerHeightConstant.constant = 170.0f - 73.0f;
        } else {
            self._scrollViewHeightConstant.constant = 73.0f;
            self._textContainerHeightConstant.constant = 170.0f;
            [self._scrollView removeAllItems];
            __block UIImageView *imageView = nil;
            [self._scrollView removeAllItems];
            [imageUrls enumerateObjectsUsingBlock:^(NSString *url, NSUInteger idx, BOOL *stop) {
                imageView = [self._imageViews objectForKey:url];
                if (!imageView) {
                    imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
                    imageView.backgroundColor = [AppColorScheme brandBlack];
                    imageView.userInteractionEnabled = YES;
                    imageView.tag = idx;
                    imageView.clipsToBounds = YES;
                    imageView.contentMode = UIViewContentModeScaleAspectFill;
                    [self._imageViews setObject:imageView forKey:url];
                }
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_handleImageViewTapEvent:)];
                [tapGesture setNumberOfTapsRequired:1];
                [imageView addGestureRecognizer:tapGesture];
                [self._scrollView addItem:imageView];
                [imageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                [imageView sd_addActivityIndicator];
                [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil options:SDWebImageRefreshCached completed:nil];
                if ([url isEqualToString:self._selectedImageUrl]) {
                    [self _addOverlayToImageView:imageView];
                }
            }];
        }
    }
    
    self._titlelbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    self._priceLbl.text = [NSString stringWithFormat:@"%@ %@", [Utils getCurrencyString:price withDecimal:NO], [LOCALIZED(@"dealer") uppercaseString]];
    self._msrpPriceLbl.text = [NSString stringWithFormat:@"%@ %@", [Utils getCurrencyString:msrpPrice withDecimal:NO], LOCALIZED(@"msrp")];
    self._conditionLbl.text = [LOCALIZED(condition) uppercaseString];
    self._conditionLbl.textColor = [BusinessUtils getVehicleConditionColor:condition];
}

@end
