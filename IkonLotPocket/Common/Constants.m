
#import "Constants.h"

///--------------------------------------
#pragma mark - Commons
///--------------------------------------

CGFloat const kDefaultMapZoomNear = 18.0f;
CGFloat const kDefaultMapZoom = 15.0f;
CGFloat const kDefaultMapZoomFar = 13.0f;
CGFloat const kBarButtonDefaultWidth = 32.0f;
CGFloat const kBarButtonDefaultHeight = 32.0f;
CGFloat const kNavigationBarDefaultHeight = 64.0f;
NSString *const kEmptyString = @"";
CGFloat const kGoldenRatio = 0.618033988749895f;

///--------------------------------------
#pragma mark - Date Format
///--------------------------------------

NSString *const kFormatDate = @"yyyy-MM-dd 00:00:00";
NSString *const kFormatDateTime = @"yyyy-MM-dd HH:mm:00.000";
NSString *const kFormatDateTimeShort = @"yyyy-MM-dd HH:mm:ss";
NSString *const kFormatDateTimeLong = @"yyyy-MM-dd HH:mm:ss.SSS";
NSString *const kFormatUSDateTime24 = @"MM/dd/yyy HH:mm";
NSString *const kFormatUSDateTimeAMPM = @"MM/dd/yyy hh:mm a";
NSString *const kFormatDateUTC = @"yyyy-MM-dd HH:mm:ss ZZZ";
NSString *const kFormatDateISO8601Short = @"yyyy-MM-dd'T'HH:mm:ss";
NSString *const kFormatDateISO8601 = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
NSString *const kFormatDateISO8601UTC = @"yyyy-MM-dd'T'HH:mm:ssZ";
NSString *const kFormatDateGMT = @"EEE, dd MMM yyyy HH:mm:ss 'GMT'";
NSString *const kFormatDayOfWeekWithDate = @"EEE, dd MMM";
NSString *const kFormatDayOfWeekWithDateTime = @"EEE, dd MMM hh:mm a";
NSString *const kFormatDayMonthShort = @"MMM/dd";
NSString *const kFormatMonthYearShort = @"MMM, yyyy";
NSString *const kFormatDateOnly = @"dd";
NSString *const kFormatDayOfWeekShort = @"EEE";
NSString *const kFormatDayOfWeekLong = @"EEEE";
NSString *const k24FormatTimeHourMinute = @"HH:mm";
NSString *const k12FormatTimeHourMinute = @"hh:mm a";
NSString *const kFormatDateUS = @"MM/dd/yyyy";

///--------------------------------------
#pragma mark - Segues & Storyboards
///--------------------------------------

NSString *const kStoryboardMain = @"Main";

NSString *const kSegueLogin = @"segue_login";
NSString *const kSegueLoginForm = @"segue_loginform";
NSString *const kSegueLanding = @"segue_landing";

NSString *const kSceneLogin = @"scene_login";
NSString *const kSceneLanding = @"scene_landing";

///--------------------------------------
#pragma mark - Preferences Keys
///--------------------------------------

NSString *const kPrefUsernameKey = @"username";
NSString *const kPrefUserIdKey = @"userid";
NSString *const kPrefPasswordKey = @"password";
NSString *const kPrefPictureUrlKey = @"pictureurl";
NSString *const kPrefIsAccountActivatedKey = @"isaccountactivated";
NSString *const kPrefSessionTokenKey = @"sessiontoken";
NSString *const kPrefAccessTokenExpiredKey = @"accesstokenexpired";
NSString *const kPrefAccessTokenCreatedKey = @"accesstokencreated";

///--------------------------------------
#pragma mark - UITableView
///--------------------------------------

NSString *const kCellTitleKey = @"celltitle";
NSString *const kCellInformationKey = @"cellinformation";
NSString *const kCellPlaceholderKey = @"cellplaceholder";
NSString *const kCellShowDisclosureIndicatorKey = @"cellshowdisclosureindicator";
NSString *const kCellColorCodeKey = @"cellstatuscolorcode";
NSString *const kCellIconImageKey = @"celliconimage";
NSString *const kCellIdentifierKey = @"cellidentifier";
NSString *const kCellIsSelectedKey = @"cellisselected";
NSString *const kCellNibNameKey = @"cellnibname";
NSString *const kCellClassKey = @"cellclass";
NSString *const kCellHeightKey = @"cellheight";
NSString *const kCellObjectDataKey = @"cellobjectdata";
NSString *const kCellTagKey = @"celltag";
NSString *const kCellItemsKey = @"items";
NSString *const kCellLabelsKey = @"labels";
NSString *const kCellValuesKey = @"values";

NSString *const kSectionTitleKey = @"sectiontitle";
NSString *const kSectionDataKey = @"sectionvalue";
CGFloat const kHeaderFooterPadding = 16.0f;
CGFloat const kHeaderFooterHeight = 36.0f;

///--------------------------------------
#pragma mark - Business
///--------------------------------------

NSString *const kExternal_SortBy_Model = @"model";
NSString *const kExternal_SortBy_HighestPrice = @"highestprice";
NSString *const kExternal_SortBy_LowestPrice = @"lowestprice";
NSString *const kExternal_SortBy_NewestYear = @"newestyear";
NSString *const kExternal_SortBy_OldestYear = @"oldestyear";
NSString *const kExternal_SortBy_HighestMileage = @"highestmileage";
NSString *const kExternal_SortBy_LowestMileage = @"lowestmileage";
NSString *const kExternal_FilterBy_Year = @"year";
NSString *const kExternal_FilterBy_PriceRange = @"price_range";
NSString *const kExternal_FilterBy_MilesRange = @"miles_range";
NSString *const kExternal_FilterBy_BodyStyle = @"body_type";
NSString *const kExternal_FilterBy_Condition = @"car_type";
NSString *const kExternal_FilterBy_Transmission = @"transmission";
NSString *const kExternal_FilterBy_FuelType = @"fuel_type";

///--------------------------------------
#pragma mark - Notifications
///--------------------------------------
