
#import "CheckMark.h"
#import "AppColorScheme.h"

@implementation CheckMark

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self _initView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self _initView];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    if (self.checked) [self drawRectChecked:rect];
    else {
        switch (self.checkMarkStyle) {
            case CheckMarkStyleOpen:[self drawRectOpenCircle:rect];
                break;
            case CheckMarkStyleGrayedOut:[self drawRectGrayedOut:rect];
                break;
            default:[self drawRectOpenCircle:rect];
                break;
        }
    }
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_initView
{
    _checked = NO;
    _checkMarkStyle = CheckMarkStyleOpen;
    _checkMarkType = CheckMarkTypeOval;
    self.backgroundColor = [UIColor clearColor];
}

///--------------------------------------
#pragma mark - Property Setters
///--------------------------------------

- (void)setChecked:(BOOL)checked
{
    _checked = checked;
    [self setNeedsDisplay];
}

- (void)setCheckMarkStyle:(CheckMarkStyle)checkMarkStyle
{
    _checkMarkStyle = checkMarkStyle;
    [self setNeedsDisplay];
}

///--------------------------------------
#pragma mark - CGGraphics Helpers
///--------------------------------------

- (void)drawRectChecked:(CGRect)rect
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();

    //// Color Declarations
    UIColor *checkmarkBlue = self.fillColor ? : [AppColorScheme brandBlue];

    //// Shadow Declarations
    UIColor *shadow2 = [UIColor blackColor];
    CGSize shadow2Offset = CGSizeMake(0.1, -0.1f);
    CGFloat shadow2BlurRadius = 2.5f;

    //// Frames
    CGRect frame = self.bounds;

    //// Subframes
    CGRect group = CGRectMake(CGRectGetMinX(frame) + 3, CGRectGetMinY(frame) + 3, CGRectGetWidth(frame) - 6,
            CGRectGetHeight(frame) - 6);

    //// Group
    {
        //// CheckedOval Drawing
        CGRect checkMarkFrame = CGRectMake(
                (CGFloat) (CGRectGetMinX(group) + floor(CGRectGetWidth(group) * 0.00000 + 0.5)),
                (CGFloat) (CGRectGetMinY(group) + floor(CGRectGetHeight(group) * 0.00000 + 0.5)),
                (CGFloat) (floor(CGRectGetWidth(group) * 1.00000 + 0.5) - floor(CGRectGetWidth(group) * 0.00000 + 0.5)),
                (CGFloat) (floor(CGRectGetHeight(group) * 1.00000 + 0.5) - floor(CGRectGetHeight(group) * 0.00000 + 0.5)));
        UIBezierPath *checkedPath = self.checkMarkType == CheckMarkTypeOval
                ? [UIBezierPath bezierPathWithOvalInRect:checkMarkFrame]
                : [UIBezierPath bezierPathWithRect:checkMarkFrame];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadow2Offset, shadow2BlurRadius, shadow2.CGColor);
        [checkmarkBlue setFill];
        [checkedPath fill];
        CGContextRestoreGState(context);

        [[UIColor whiteColor] setStroke];
        checkedPath.lineWidth = 1;
        [checkedPath stroke];

        //// Bezier Drawing
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake((CGFloat) (CGRectGetMinX(group) + 0.27083 * CGRectGetWidth(group)),
                (CGFloat) (CGRectGetMinY(group) + 0.54167 * CGRectGetHeight(group)))];
        [bezierPath addLineToPoint:CGPointMake((CGFloat) (CGRectGetMinX(group) + 0.41667 * CGRectGetWidth(group)),
                (CGFloat) (CGRectGetMinY(group) + 0.68750 * CGRectGetHeight(group)))];
        [bezierPath addLineToPoint:CGPointMake((CGFloat) (CGRectGetMinX(group) + 0.75000 * CGRectGetWidth(group)),
                (CGFloat) (CGRectGetMinY(group) + 0.35417 * CGRectGetHeight(group)))];
        bezierPath.lineCapStyle = kCGLineCapSquare;

        [[UIColor whiteColor] setStroke];
        bezierPath.lineWidth = 1.3f;
        [bezierPath stroke];
    }
}

- (void)drawRectGrayedOut:(CGRect)rect
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();

    //// Color Declarations
    UIColor *grayTranslucent = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.6f];

    //// Shadow Declarations
    UIColor *shadow2 = [UIColor blackColor];
    CGSize shadow2Offset = CGSizeMake(0.1f, -0.1f);
    CGFloat shadow2BlurRadius = 2.5f;

    //// Frames
    CGRect frame = self.bounds;

    //// Subframes
    CGRect group = CGRectMake(CGRectGetMinX(frame) + 3, CGRectGetMinY(frame) + 3, CGRectGetWidth(frame) - 6,
            CGRectGetHeight(frame) - 6);

    //// Group
    {
        //// UncheckedOval Drawing
        CGRect checkMarkFrame = CGRectMake(
                (CGFloat) (CGRectGetMinX(group) + floor(CGRectGetWidth(group) * 0.00000 + 0.5)),
                (CGFloat) (CGRectGetMinY(group) + floor(CGRectGetHeight(group) * 0.00000 + 0.5)),
                (CGFloat) (floor(CGRectGetWidth(group) * 1.00000 + 0.5) - floor(CGRectGetWidth(group) * 0.00000 + 0.5)),
                (CGFloat) (floor(CGRectGetHeight(group) * 1.00000 + 0.5) - floor(CGRectGetHeight(group) * 0.00000 + 0.5)));
        UIBezierPath *uncheckedPath = self.checkMarkType == CheckMarkTypeOval
                ? [UIBezierPath bezierPathWithOvalInRect:checkMarkFrame]
                : [UIBezierPath bezierPathWithRect:checkMarkFrame];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadow2Offset, shadow2BlurRadius, shadow2.CGColor);
        [grayTranslucent setFill];
        [uncheckedPath fill];
        CGContextRestoreGState(context);

        [[UIColor whiteColor] setStroke];
        uncheckedPath.lineWidth = 1;
        [uncheckedPath stroke];

        //// Bezier Drawing
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake((CGFloat) (CGRectGetMinX(group) + 0.27083 * CGRectGetWidth(group)),
                (CGFloat) (CGRectGetMinY(group) + 0.54167 * CGRectGetHeight(group)))];
        [bezierPath addLineToPoint:CGPointMake((CGFloat) (CGRectGetMinX(group) + 0.41667 * CGRectGetWidth(group)),
                (CGFloat) (CGRectGetMinY(group) + 0.68750 * CGRectGetHeight(group)))];
        [bezierPath addLineToPoint:CGPointMake((CGFloat) (CGRectGetMinX(group) + 0.75000 * CGRectGetWidth(group)),
                (CGFloat) (CGRectGetMinY(group) + 0.35417 * CGRectGetHeight(group)))];
        bezierPath.lineCapStyle = kCGLineCapSquare;

        [[UIColor whiteColor] setStroke];
        bezierPath.lineWidth = 1.3;
        [bezierPath stroke];
    }
}

- (void)drawRectOpenCircle:(CGRect)rect
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();

    //// Shadow Declarations
    UIColor *shadow = [UIColor blackColor];
    CGSize shadowOffset = CGSizeMake(0.1f, -0.1f);
    CGFloat shadowBlurRadius = 0.5f;
    UIColor *shadow2 = [UIColor blackColor];
    CGSize shadow2Offset = CGSizeMake(0.1f, -0.1f);
    CGFloat shadow2BlurRadius = 2.5f;

    //// Frames
    CGRect frame = self.bounds;

    //// Subframes
    CGRect group = CGRectMake(CGRectGetMinX(frame) + 3, CGRectGetMinY(frame) + 3, CGRectGetWidth(frame) - 6,
            CGRectGetHeight(frame) - 6);

    //// Group
    {
        //// EmptyOval Drawing
        CGRect checkMarkFrame = CGRectMake(
                (CGFloat) (CGRectGetMinX(group) + floor(CGRectGetWidth(group) * 0.00000 + 0.5)),
                (CGFloat) (CGRectGetMinY(group) + floor(CGRectGetHeight(group) * 0.00000 + 0.5)),
                (CGFloat) (floor(CGRectGetWidth(group) * 1.00000 + 0.5) - floor(CGRectGetWidth(group) * 0.00000 + 0.5)),
                (CGFloat) (floor(CGRectGetHeight(group) * 1.00000 + 0.5) - floor(CGRectGetHeight(group) * 0.00000 + 0.5)));
        UIBezierPath *emptyPath = self.checkMarkType == CheckMarkTypeOval
                ? [UIBezierPath bezierPathWithOvalInRect:checkMarkFrame]
                : [UIBezierPath bezierPathWithRect:checkMarkFrame];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadow2Offset, shadow2BlurRadius, shadow2.CGColor);
        CGContextRestoreGState(context);

        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
        [[UIColor whiteColor] setStroke];
        emptyPath.lineWidth = 1.5;
        [emptyPath stroke];
        CGContextRestoreGState(context);
    }
}

@end
