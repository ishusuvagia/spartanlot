
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const __nonnull kNavigationPresent;
extern NSString *const __nonnull kNavigationPush;
extern NSString *const __nonnull kSeparatorCharacter;

@interface LinkedStoryboardSegue : UIStoryboardSegue

+ (nonnull UIViewController *)sceneNamed:(nonnull NSString *)identifier;

@end

NS_ASSUME_NONNULL_END
