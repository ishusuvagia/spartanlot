
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CheckMarkStyle)
{
    CheckMarkStyleOpen,
    CheckMarkStyleGrayedOut
};

typedef NS_ENUM(NSInteger, CheckMarkType)
{
    CheckMarkTypeOval,
    CheckMarkTypeSquare
};

@interface CheckMark : UIControl

@property (nonatomic, assign) BOOL checked;
@property (nonatomic, assign) CheckMarkStyle checkMarkStyle;
@property (nonatomic, assign) CheckMarkType checkMarkType;
@property (nullable, nonatomic, strong) NSString *associatedText;
@property (nullable, nonatomic, strong) NSIndexPath *indexPath;
@property (nullable, nonatomic, strong) UIColor *fillColor;

@end

NS_ASSUME_NONNULL_END
