
#import "CustomActivityButton.h"
#import "Constants.h"
#import "Logger.h"

@interface CustomActivityButton ()

@property (nonatomic, assign, readwrite) BOOL isLoading;
@property (nonatomic, readonly) UIActivityIndicatorView *_activity;
@property (nonatomic, copy) NSString *_titleText;
@property (nonatomic, copy) UIImage *_currentImage;

@end

@implementation CustomActivityButton

@synthesize _activity = __activity;

- (void)customize
{
    [super customize];

    _activityStyle = UIActivityIndicatorViewStyleWhite;
    _isLoading = NO;
    __titleText = kEmptyString;
}

- (UIActivityIndicatorView *)_activity
{
    if (!__activity) {
        __activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:self.activityStyle];
        CGFloat height = self.bounds.size.height / 2;
        CGFloat width = self.bounds.size.width / 2;
        self._activity.center = CGPointMake(width, height);
        [self addSubview:__activity];
    }
    return __activity;
}

- (void)dealloc
{
    TRACE_HERE;
    __activity = nil;
    self._titleText = nil;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setActivityIndicatorStyle:(UIActivityIndicatorViewStyle)style
{
    if (style == UIActivityIndicatorViewStyleWhiteLarge)
        style = UIActivityIndicatorViewStyleWhite;
    self._activity.activityIndicatorViewStyle = style;
}

- (void)showActivity
{
    [self setTitle:kEmptyString forState:UIControlStateNormal];
    [self._activity startAnimating];
    self.enabled = NO;
    self.isLoading = YES;
    self._titleText = self.titleLabel.text;
    self._currentImage = self.currentImage;
}

- (void)hideActivity
{
    self.enabled = YES;
    self.isLoading = NO;
    self.imageView.alpha = 1.0f;
    if (self._titleText.length > 0) [self setTitle:self._titleText forState:UIControlStateNormal];
    [self setImage:self._currentImage forState:UIControlStateNormal];
    self._titleText = kEmptyString;
    self._currentImage = nil;
    [self._activity stopAnimating];
}

@end
