
#import "RestRequest.h"

@interface RestRequest ()

@property (nonatomic, copy, readwrite) NSString *httpPath;
@property (nonatomic, copy, readwrite) NSString *httpMethod;
@property (nonatomic, copy, readwrite) id httpBody;

@end

@implementation RestRequest

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)requestObjectWithHTTPPath:(NSString *)path
                               httpMethod:(NSString *)method
                                 bodyData:(id)bodyData
{
    RestRequest *restRequest = [RestRequest new];
    restRequest.httpPath = path;
    restRequest.httpMethod = method;
    restRequest.httpBody = bodyData;
    return restRequest;
}

+ (instancetype)requestObjectWithHTTPPath:(NSString *)path
                               httpMethod:(NSString *)method
                                 bodyData:(id)bodyData
                        additionalHeaders:(NSDictionary *)additionalHeaders
{
    RestRequest *restRequest = [RestRequest requestObjectWithHTTPPath:path httpMethod:method bodyData:bodyData];
    restRequest.additionalHeaders = [additionalHeaders mutableCopy];
    return restRequest;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
        _additionalHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)dealloc
{
    self.httpPath = nil;
    self.httpMethod = nil;
    self.httpBody = nil;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"\nPath=%@\nMethod=%@\nBody=%@\\n",
                             self.httpPath, self.httpMethod, self.httpBody];
}

@end
