
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RestResponse : NSObject

@property (nullable, nonatomic, strong, readonly) id result;
@property (nonatomic, assign, readonly) BOOL isSuccess;
@property (nonatomic, assign, readonly) NSInteger headerStatusCode;
@property (nonnull, nonatomic, copy, readonly) NSDictionary *allHeaderFields;
@property (nullable, nonatomic, copy, readonly) NSString *statusMessage;

+ (nonnull instancetype)reponseObjectWithResult:(nullable id)result
                                      isSuccess:(BOOL)isSuccess
                                     statusCode:(NSInteger)statusCode
                                allHeaderFields:(nonnull NSDictionary *)allHeaderFields
                                  statusMessage:(nullable NSString *)statusMessage;


@end

NS_ASSUME_NONNULL_END
