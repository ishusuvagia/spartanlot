
#import "GeofenceManager.h"
#import "Logger.h"

static const NSTimeInterval kMaxTimeToProcessGeofences = 6.0f;
static const NSUInteger kGeofenceMonitoringLimit = 20;
static NSString *const kCurrentRegionName = @"currentRegion";
static const CGFloat kCurrentRegionPaddingRatio = 0.5f;
static NSString *const kInsideRegionsDefaultsKey = @"insideregionsdefaultskey";

@interface GeofenceManager () <CLLocationManagerDelegate>
{
    BOOL _isTransitioning;
}

@property (nonatomic, strong) CLLocationManager *_locationManager;
@property (nonatomic, strong) NSArray *_allGeofences;
@property (nonatomic, strong) NSMutableDictionary *_regionsGroupedByDistance;
@property (nonatomic, strong) NSMutableArray *_regionsBeingProcessed;
@property (nonatomic, strong) NSMutableIndexSet *_boundaryIndicesBeingProcessed;
@property (nonatomic, strong) NSMutableSet *_nearestRegions;
@property (nonatomic, strong) NSMutableSet *_insideRegions;
@property (nonatomic, strong) NSMutableArray *_previouslyInsideRegionIds;
@property (nonatomic, strong) NSTimer *_processingTimer;

@end

@implementation GeofenceManager

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)sharedManager
{
    static GeofenceManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [GeofenceManager new];
    });
    return manager;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
        self._locationManager = [CLLocationManager new];
        self._locationManager.delegate = self;
        self._locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *previouslyInsideRegionIds = [defaults arrayForKey:kInsideRegionsDefaultsKey];
        self._previouslyInsideRegionIds = [previouslyInsideRegionIds mutableCopy];
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_setState:(GeofenceManagerState)state
{
    if (state != self.state) {
        self.state = state;
        if ([self.delegate respondsToSelector:@selector(geofenceManager:didChangeState:)]) {
            [self.delegate geofenceManager:self didChangeState:self.state];
        }
    }
}

- (void)_transitionReloadGeofences
{
    _isTransitioning = YES;
    [self _reloadGeofences];
}

- (void)_reloadGeofences
{
    [self._processingTimer invalidate];
    [self._locationManager stopUpdatingLocation];
    [self._locationManager stopMonitoringSignificantLocationChanges];

    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
        if ([self._locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self._locationManager requestAlwaysAuthorization];
            return;
        }
    }

    self._regionsGroupedByDistance = nil;
    self._regionsBeingProcessed = nil;

    for (CLRegion *region in [self._locationManager monitoredRegions]) {
        [self._locationManager stopMonitoringForRegion:region];
    }

    self._allGeofences = [self.dataSource geofencesForGeofenceManager:self];

    if ([self._allGeofences count] > 0) {
        [self _setState:GeofenceManagerStateProcessing];

        // Timer to get a lock on the GPS location
        NSTimeInterval timeToLock = 10 - kMaxTimeToProcessGeofences;
        self._processingTimer = [NSTimer scheduledTimerWithTimeInterval:timeToLock
                                                                 target:self
                                                               selector:@selector(_startProcessingGeofences)
                                                               userInfo:nil
                                                                repeats:NO];

        // Turn on location updates for accuracy and so processing can happen in the background.
        [self._locationManager stopUpdatingLocation];
        [self._locationManager startUpdatingLocation];

        // Turn on significant location changes to help monitor the current region.
        [self._locationManager startMonitoringSignificantLocationChanges];
    } else {
        [self _setState:GeofenceManagerStateIdle];
    }
}

- (void)_startProcessingGeofences
{
    TRACE("You are near %@", self._locationManager.location);
    self._processingTimer = [NSTimer scheduledTimerWithTimeInterval:kMaxTimeToProcessGeofences
                                                             target:self
                                                           selector:@selector(_failedProcessingGeofencesWithError:)
                                                           userInfo:nil
                                                            repeats:NO];
    [self _processFencesNearLocation:self._locationManager.location];
}

- (void)_processFencesNearLocation:(CLLocation *)location
{
    if (self.state != GeofenceManagerStateProcessing) {
        return;
    }

    self._boundaryIndicesBeingProcessed = [NSMutableIndexSet indexSet];
    self._regionsGroupedByDistance = [NSMutableDictionary dictionary];
    self._regionsBeingProcessed = [NSMutableArray array];
    self._nearestRegions = [NSMutableSet set];
    self._insideRegions = [NSMutableSet set];

    NSMutableArray *fencesWithDistanceToBoundary = [NSMutableArray array];
    NSMutableDictionary *minBoundaryDistancesByIndex = [NSMutableDictionary dictionary];

    for (CLCircularRegion *fence in self._allGeofences) {
        if (fence.radius < self._locationManager.maximumRegionMonitoringDistance) {
            CLLocation *fenceCenter =
                    [[CLLocation alloc] initWithLatitude:fence.center.latitude longitude:fence.center.longitude];

            // CLLocationAccuracy accuracy = location.horizontalAccuracy;
            CLLocationDistance d_r = [location distanceFromLocation:fenceCenter] - fence.radius;
            [fencesWithDistanceToBoundary addObject:@[fence, @(fabs(d_r))]];

            if (d_r < 0) {
                [self._insideRegions addObject:fence];
            } else {
                int rounded = (int) d_r;
                rounded -= rounded % 10;

                if (rounded <= 0) {
                    [self._insideRegions addObject:fence];
                } else if (rounded <= 200) { // Group by distances within 10m of eachother, but no more than 200m away from user.
                    NSNumber *key = @(rounded);
                    NSArray *val = self._regionsGroupedByDistance[key];
                    if (val) {
                        if ([minBoundaryDistancesByIndex[key] compare:@(d_r)] == NSOrderedDescending) {
                            val = [@[fence] arrayByAddingObjectsFromArray:val];
                            minBoundaryDistancesByIndex[key] = @(d_r);
                        } else {
                            val = [val arrayByAddingObject:fence];
                        }
                    } else {
                        val = @[fence];
                        minBoundaryDistancesByIndex[key] = @(d_r);
                    }
                    self._regionsGroupedByDistance[key] = val;
                }
            }
        }
    }

    [fencesWithDistanceToBoundary sortUsingComparator:^NSComparisonResult(NSArray *tuple1, NSArray *tuple2) {
        return [[tuple1 lastObject] compare:[tuple2 lastObject]];
    }];

    [fencesWithDistanceToBoundary enumerateObjectsUsingBlock:^(NSArray *tuple, NSUInteger idx, BOOL *stop) {
        CLRegion *fence = [tuple firstObject];
        if (idx < kGeofenceMonitoringLimit - 1) {
            [self._nearestRegions addObject:fence];
        } else {
            *stop = YES;
        }
    }];

    if ([self._nearestRegions count] == kGeofenceMonitoringLimit - 1) {
        // We need a region around the user to refresh geofences.
        NSArray *tuple = [fencesWithDistanceToBoundary lastObject];
        CLLocationDistance
                radius = MIN(self._locationManager.maximumRegionMonitoringDistance, [[tuple lastObject] doubleValue]);
        radius = MAX(radius, 2.0) * kCurrentRegionPaddingRatio;

        CLCircularRegion *currentRegion =
                [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius
                                              identifier:kCurrentRegionName];
        [self._regionsBeingProcessed addObject:currentRegion];
        [self._boundaryIndicesBeingProcessed addIndex:0];
    } else {
        [self._regionsBeingProcessed addObject:[NSNull null]];
    }

    for (NSUInteger i = 1; i <= 20; i++) {
        NSNumber *key = @(10 * i);
        NSArray *val = self._regionsGroupedByDistance[key];
        if (val) {
            CLRegion *fence = [val firstObject];
            [self._regionsBeingProcessed addObject:fence];
            [self._boundaryIndicesBeingProcessed addIndex:i];
        } else {
            [self._regionsBeingProcessed addObject:[NSNull null]];
        }
    }

    if ([self._boundaryIndicesBeingProcessed count] == 0) {
        for (CLRegion *fence in self._nearestRegions) {
            [self._locationManager startMonitoringForRegion:fence];
        }
    } else {
        for (id fence in self._regionsBeingProcessed) {
            if ([fence isKindOfClass:[CLRegion class]]) {
                [self._locationManager startMonitoringForRegion:fence];
            }
        }
    }
}

- (void)_failedProcessingGeofencesWithError:(NSError *)error
{
    [self._processingTimer invalidate];
    [self._locationManager stopUpdatingLocation];
    self._regionsGroupedByDistance = nil;
    self._regionsBeingProcessed = nil;
    [self _handleGeofenceEvents];
    [self _setState:GeofenceManagerStateFailed];

    if ([self.delegate respondsToSelector:@selector(geofenceManager:didFailWithError:)]) {
        if ([error isKindOfClass:[NSError class]]) {
            [self.delegate geofenceManager:self didFailWithError:error];
        } else {
            NSError *timeoutError =
                    [NSError errorWithDomain:@"Geofence manager timed out" code:kCFURLErrorTimedOut userInfo:nil];
            [self.delegate geofenceManager:self didFailWithError:timeoutError];
        }
    }
}

- (void)_finishedProcessingGeofences
{
    [self._processingTimer invalidate];
    [self._locationManager stopUpdatingLocation];
    self._regionsGroupedByDistance = nil;
    self._regionsBeingProcessed = nil;
    [self _handleGeofenceEvents];
    [self _setState:GeofenceManagerStateIdle];
}

- (void)_handleGeofenceEvents
{
    NSMutableArray *insideRegionIds = [NSMutableArray arrayWithCapacity:[self._insideRegions count]];

    for (CLRegion *region in self._insideRegions) {
        [insideRegionIds addObject:region.identifier];
        if ([self.delegate respondsToSelector:@selector(geofenceManager:isInsideGeofence:)]) {
            if (_isTransitioning) {
                if (![self._previouslyInsideRegionIds containsObject:region.identifier]) {
                    [self.delegate geofenceManager:self isInsideGeofence:region];
                }
            } else {
                [self.delegate geofenceManager:self isInsideGeofence:region];
            }
        }
    }

    if (_isTransitioning && [self.delegate respondsToSelector:@selector(geofenceManager:didExitGeofence:)]) {
        for (CLRegion *region in self._allGeofences) {
            if ([self._insideRegions containsObject:region]) {
                continue;
            }

            if ([self._previouslyInsideRegionIds containsObject:region.identifier]) {
                [self.delegate geofenceManager:self didExitGeofence:region];
            }
        }
    }

    self._previouslyInsideRegionIds = insideRegionIds;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:insideRegionIds forKey:kInsideRegionsDefaultsKey];
    [defaults synchronize];
}

///--------------------------------------
#pragma mark - <CLLocationManagerDelegate>
///--------------------------------------

- (void)   locationManager:(CLLocationManager *)manager
monitoringDidFailForRegion:(CLRegion *)region
                 withError:(NSError *)error
{
    if (self.state != GeofenceManagerStateProcessing) {
        return;
    } else if (![self._regionsBeingProcessed containsObject:region] && ![self._nearestRegions containsObject:region]) {
        return;
    }

    if ([CLLocationManager respondsToSelector:@selector(isMonitoringAvailableForClass:)]) {
        if (![CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) { // Old iOS
            [self _failedProcessingGeofencesWithError:error];
            return;
        }
    }

    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied ||
            [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) { // You need to authorize Location Services for the APP
        [self _failedProcessingGeofencesWithError:error];
        return;
    }

    TRACE("Try again %@", region.identifier);
    [manager performSelectorInBackground:@selector(startMonitoringForRegion:) withObject:region];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    if (![region.identifier isEqualToString:kCurrentRegionName] && self.state != GeofenceManagerStateProcessing) {
        [self _transitionReloadGeofences];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    if ([region.identifier isEqualToString:kCurrentRegionName] && self.state != GeofenceManagerStateProcessing) { // We exited the current region, so we need to refresh.
        [self _transitionReloadGeofences];
    } else {
        [self._previouslyInsideRegionIds removeObject:region.identifier];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self._previouslyInsideRegionIds forKey:kInsideRegionsDefaultsKey];
        [defaults synchronize];

        if ([self.delegate respondsToSelector:@selector(geofenceManager:didExitGeofence:)]) { // Exited a geofence.
            [self.delegate geofenceManager:self didExitGeofence:region];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    if (self.state != GeofenceManagerStateProcessing) {
        return;
    }

    NSUInteger idx = [self._regionsBeingProcessed indexOfObject:region];

    if (idx == NSNotFound) {
        return;
    }

    if ([region.identifier isEqualToString:kCurrentRegionName]) {
        if (state == CLRegionStateInside) { // Keep attempting to find the current region.
            TRACE("Found current region %@", region);
        } else {
            CLLocationDistance radius = [(CLCircularRegion *) region radius];
            CLCircularRegion *currentRegion =
                    [[CLCircularRegion alloc] initWithCenter:manager.location.coordinate radius:radius
                                                  identifier:kCurrentRegionName];
            self._regionsBeingProcessed[idx] = currentRegion;
            [manager performSelectorInBackground:@selector(startMonitoringForRegion:) withObject:currentRegion];
            return;
        }
    } else {
        NSNumber *key = @(10 * idx);
        if (state == CLRegionStateInside) {
            NSArray *fences = self._regionsGroupedByDistance[key];
            [self._insideRegions addObjectsFromArray:fences];
        }

        if ([self._nearestRegions containsObject:region]) {
            [self._nearestRegions removeObject:region];
        } else {
            [manager stopMonitoringForRegion:region];
        }
        TRACE("Processed %@ - %@m", region.identifier, key);
    }

    self._regionsBeingProcessed[idx] = [NSNull null];
    [self._boundaryIndicesBeingProcessed removeIndex:idx];

    if ([self._boundaryIndicesBeingProcessed count] == 0) {
        for (CLRegion *fence in self._nearestRegions) {
            [manager performSelectorInBackground:@selector(startMonitoringForRegion:) withObject:fence];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    if (self.state != GeofenceManagerStateProcessing) {
        return;
    }

    if ([self._boundaryIndicesBeingProcessed count] == 0) {
        [self._nearestRegions removeObject:region];
        if ([self._nearestRegions count] == 0) { // All regions have finished processing, finish up.
            [self _finishedProcessingGeofences];
        }
    } else if ([manager respondsToSelector:@selector(requestStateForRegion:)]) {
        [manager requestStateForRegion:region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied ||
            [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) {
        [self _failedProcessingGeofencesWithError:error];
        return;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (self.state != GeofenceManagerStateProcessing) {
        [self _transitionReloadGeofences];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        [self _reloadGeofences];
    }
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)reloadGeofences
{
    _isTransitioning = NO;
    [self _reloadGeofences];
}

- (NSString *)geofenceStateDescription
{
    switch (self.state) {
        case GeofenceManagerStateIdle: return @"GeofenceManager State Idle";
        case GeofenceManagerStateProcessing: return @"GeofenceManager State Processing";
        case GeofenceManagerStateFailed: return @"GeofenceManager State Failed";
        default: return @"GeofenceManager State Unrecognized";
    }
}

@end
