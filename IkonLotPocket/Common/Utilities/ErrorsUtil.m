
#import "ErrorsUtil.h"

NSString *const GeneralErrorDomain = @"IkonGPS";
NSString *const WebRequestErrorDomain = @"IkonGPS.WebRequest";

NSString *const ErrorCodeKey = @"ErrorCode";
NSString *const ErrorMessageKey = @"ErrorMessage";
NSString *const ErrorDomainKey = @"ErrorDomain";

@implementation ErrorsUtil

+ (NSError *)errorWithCode:(ErrorCode)code message:(NSString *)message
{
    return [self errorWithCode:code message:message shouldLog:YES];
}

+ (NSError *)errorWithCode:(ErrorCode)code message:(NSString *)message shouldLog:(BOOL)shouldLog
{
    NSDictionary *result =
            @{ErrorCodeKey: @(code),
                    ErrorMessageKey: message ?: [self getErrorMessage:code],
                    ErrorDomainKey: GeneralErrorDomain};
    return [self errorFromResult:result shouldLog:shouldLog];
}

+ (NSError *)errorWithCode:(ErrorCode)code domain:(NSString *)domain message:(NSString *)message
{
    return [self errorWithCode:code domain:domain message:message shouldLog:YES];
}

+ (NSError *)errorWithCode:(ErrorCode)code domain:(NSString *)domain message:(NSString *)message shouldLog:(BOOL)shouldLog
{
    NSDictionary *result = @{ErrorCodeKey: @(code), ErrorMessageKey: message ?: [self getErrorMessage:code], ErrorDomainKey: domain};
    return [self errorFromResult:result shouldLog:shouldLog];
}

+ (NSError *)errorFromResult:(NSDictionary *)result
{
    return [self errorFromResult:result shouldLog:YES];
}

+ (NSError *)errorFromResult:(NSDictionary *)result shouldLog:(BOOL)shouldLog
{
    NSInteger errorCode = [result[ErrorCodeKey] integerValue];
    NSString *errorMessage = result[ErrorMessageKey];

    if (shouldLog) {
        NSLog(@"%@ (Code: %ld)", errorMessage, (long) errorCode);
    }

    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:result];
    if (errorMessage) {
        userInfo[NSLocalizedDescriptionKey] = errorMessage;
    }

    NSString *errorDomain = result[ErrorDomainKey] ?: GeneralErrorDomain;

    return [NSError errorWithDomain:errorDomain code:errorCode userInfo:userInfo];
}

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSString *)getErrorMessage:(ErrorCode)code
{
    NSString *errorString;

    switch (code) {
        case kErrorParsingException:
            errorString = @"Has exception in data parsing.";
            break;
        case kErrorAPICompatible:
            errorString = @"Response is not valid.";
            break;
        case kErrorAPIInconsistentData:
            errorString = @"Object is not parsed due to the inconsistency of API payload.";
            break;
        case kErrorAPINullData:
            errorString = @"Object is null.";
            break;
        case kErrorInternalServer:
            errorString = @"API internal error.";
            break;
        default:
            errorString = @"Unknown error.";
            break;
    }

    return errorString;
}

@end
