
#import <objc/runtime.h>
#import <ECPhoneNumberFormatter/ECPhoneNumberFormatter.h>
#import "Utils.h"
#import "Logger.h"

@implementation Utils

///--------------------------------------
#pragma mark - Strings
///--------------------------------------

+ (NSString *)getBooleanString:(BOOL)value
{
    return value ? @"true" : @"false";
}

+ (NSString *)formatFirstname:(NSString *)firstname andLastName:(NSString *)lastname
{
    return [NSString stringWithFormat:@"%@ %@", firstname, lastname];
}

+ (BOOL)stringIsNullOrEmpty:(NSString *)string
{
    return (!string || [string isKindOfClass:[NSNull class]] || [string isEqualToString:kEmptyString]);
}

+ (NSDictionary *)convertToJsonDict:(NSString *)value
{
    if (value) {
        NSData *data = [value dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        return dict;
    }
    return nil;
}

+ (NSString *)convertToJsonString:(NSDictionary *)dict
{
    if (dict) {
        NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:(NSJSONWritingOptions) kNilOptions error:nil];
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        return result;
    }
    return nil;
}

+ (NSString *)getFormattedPhoneNumber:(NSString *)phoneNumber
{
    if ([self stringIsNullOrEmpty:phoneNumber] || ![self validatePhoneNumber:phoneNumber]) {
        return kEmptyString;
    }
    ECPhoneNumberFormatter *formatter = [ECPhoneNumberFormatter new];
    NSString *formattedNumber = [formatter stringForObjectValue:phoneNumber];
    return formattedNumber;
}

+ (NSString *)getUnformattedPhoneNumber:(NSString *)formattedPhoneNumber
{
    if ([self stringIsNullOrEmpty:formattedPhoneNumber] || ![self validatePhoneNumber:formattedPhoneNumber]) {
        return kEmptyString;
    }
    ECPhoneNumberFormatter *formatter = [[ECPhoneNumberFormatter alloc] init];
    id objectValue;
    NSString *error;
    [formatter getObjectValue:&objectValue forString:formattedPhoneNumber errorDescription:&error];
    return !error ? objectValue : kEmptyString;
}

+ (NSString *)getCurrencyString:(CGFloat)currencyAmount withDecimal:(BOOL)withDecimal
{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    if (!withDecimal) [numberFormatter setMaximumFractionDigits:0];
    NSString *amount = [numberFormatter stringFromNumber:@(currencyAmount)];
    return amount;
}

+ (NSString *)getNumberString:(CGFloat)number
{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *amount = [numberFormatter stringFromNumber:@(number)];
    return amount;
}

+ (NSString *)capitalizedOnlyFirstLetter:(NSString *)string
{
    if (string.length < 1) {
        return kEmptyString;
    } else if (string.length == 1) {
        return [string capitalizedString];
    } else {
        if ([string.uppercaseString isEqualToString:string]) {
            return string;
        } else {
            NSString *firstChar = [string substringToIndex:1];
            NSString *otherChars = [string substringWithRange:NSMakeRange(1, string.length - 1)];
            return [NSString stringWithFormat:@"%@%@", [firstChar uppercaseString], [otherChars lowercaseString]];
        }
    }
}

+ (NSString *)formatCommaSpaceValue:(NSArray *)values
{
    NSString *result = kEmptyString;
    for (NSUInteger idx = 0; idx < values.count; idx++) {
        NSString *value = values[idx];
        if (![self stringIsNullOrEmpty:value]) {
            if (idx < values.count - 1) {
                result = [result stringByAppendingString:[NSString stringWithFormat:@"%@,", value]];
            } else {
                result = [result stringByAppendingString:value];
            }
        }
    }
    return result;
}

+ (NSArray *)commaSpaceValueToArray:(NSString *)value
{
    if (![Utils stringIsNullOrEmpty:value]) {
        return [value componentsSeparatedByString:@","];
    } else {
        return @[];
    }
}

///--------------------------------------
/// @name Maths
///--------------------------------------

+ (double)calculateDistanceInMetersBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2
{
    NSInteger nRadius = 6371; // Earth's radius in Kilometers
    double latDiff = (coord2.latitude - coord1.latitude) * (M_PI / 180);
    double lonDiff = (coord2.longitude - coord1.longitude) * (M_PI / 180);
    double lat1InRadians = coord1.latitude * (M_PI / 180);
    double lat2InRadians = coord2.latitude * (M_PI / 180);
    double nA = pow(sin(latDiff / 2), 2) + cos(lat1InRadians) * cos(lat2InRadians) * pow(sin(lonDiff / 2), 2);
    double nC = 2 * atan2(sqrt(nA), sqrt(1 - nA));
    double nD = nRadius * nC;
    // convert to meters
    return nD * 1000;
}

+ (double)calculateDistanceInMilesBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2
{
    double distanceInMeter = [self calculateDistanceInMetersBetweenCoord:coord1 coord:coord2];
    return distanceInMeter * 0.000621371;
}

///--------------------------------------
#pragma mark - System
///--------------------------------------

+ (id)objectOrNull:(id)object
{
    return object ?: [NSNull null];
}

+ (void)saveDictionaryToUserDefaults:(NSDictionary *)dict
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    for (NSString *key in dict.allKeys) {
        [userDefaults setObject:dict[key] forKey:key];
    }
    [userDefaults synchronize];
}

+ (void)saveObjectToUserDefaults:(id)object forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:object forKey:key];
    [userDefaults synchronize];
}

+ (id)getObjectFromUserDefaults:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:key];
}

+ (void)removeObjectFromUserDefaults:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:key];
}

+ (NSDictionary *)dictWithPropertiesOfObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);

    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        Class classObject = NSClassFromString([key capitalizedString]);
        id object = [obj valueForKey:key];

        if (classObject) {
            id subObj = [self dictWithPropertiesOfObject:object];
            dict[key] = subObj;
        } else if ([object isKindOfClass:[NSArray class]]) {
            NSMutableArray *subObj = [NSMutableArray array];
            for (id o in object) {
                [subObj addObject:[self dictWithPropertiesOfObject:o]];
            }
            dict[key] = subObj;
        } else {
            if (object) dict[key] = object;
        }
    }

    free(properties);

    return [NSDictionary dictionaryWithDictionary:dict];
}

+ (NSMutableDictionary *)getPlist:(NSString *)plistName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    return dict;
}

+ (NSMutableDictionary *)getParamPreferences:(NSString *)plistName
{
    return [self getParamPreferences:plistName filterKeys:nil];
}

+ (NSMutableDictionary *)getParamPreferences:(NSString *)plistName filterKeys:(NSArray *)filterKeys
{
    // Get the default preferences from plist file name
    NSMutableDictionary *dict = [self getPlist:plistName];

    if (dict) {
        NSMutableDictionary *result = nil;

        // Apply filtered keys (provided).
        if (filterKeys) {
            result = [NSMutableDictionary dictionaryWithCapacity:filterKeys.count];
            for (NSString *key in filterKeys) {
                if (dict[key]) result[key] = dict[key];
            }
        } else {
            result = dict;
        }

        // Copy data from NSUserDefaults (if any) to the dictionary. This way
        // the result will be up to date. These values only reset when the
        // user removes the app.
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        for (NSString *key in [result allKeys]) {
            if (![userDefaults objectForKey:key]) {
                [userDefaults setObject:result[key] forKey:key];
                [userDefaults synchronize];
            } else {
                result[key] = [userDefaults objectForKey:key];
            }
        }

        return result;
    } else {
        return [NSMutableDictionary dictionary];
    }
}

+ (NSURL *)getLocalDirectory:(NSString *)folderName
{
    if ([self stringIsNullOrEmpty:folderName]) {
        return nil;
    }

    NSString *documentDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *folderPath = [documentDirectory stringByAppendingPathComponent:folderName];

    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        TRACE("Create new folder: %@", folderName);
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil
                                                        error:nil];
    }

    return [NSURL URLWithString:folderPath];
}

///--------------------------------------
#pragma mark - Date
///--------------------------------------

+ (BOOL)isDayTime
{
    NSString *currentTimeStr = [Utils convertNSDateToString:[NSDate date] withFormat:@"HH.mm"];
    if ([currentTimeStr floatValue] >= 18.0f || [currentTimeStr floatValue] <= 6.0f) {
        return NO;
    } else {
        return YES;
    }
}

+ (NSInteger)getYear:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    NSInteger year = [components year];
    return year;
}

+ (NSInteger)getMonth:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    NSInteger month = [components month];
    return month;
}

+ (NSInteger)getDay:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    NSInteger day = [components day];
    return day;
}

+ (NSDate *)getDateOnly:(NSDate *)date
{
    NSString *strBusinessDate = [self convertNSDateToString:date];
    NSDate *dateOnly = [self convertStringToNSDate:strBusinessDate];
    return dateOnly;
}

+ (NSDate *)getStartOfBusinessWeek:(NSDate *)date;
{
    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSDateComponents *components =
            [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday
                        fromDate:date];
    [components setWeekday:1];
    [components setWeekOfYear:[components weekOfYear]];

    NSDate *rawDate = [calendar dateFromComponents:components];

    return [Utils getDateOnly:rawDate];
}

+ (NSDate *)nextWeekFromDate:(NSDate *)date
{
    NSTimeInterval secondsInWeek = 7 * 24 * 60 * 60;
    NSDate *nextWeek = [NSDate dateWithTimeInterval:secondsInWeek sinceDate:date];
    return nextWeek;
}

+ (NSDate *)lastWeekFromDate:(NSDate *)date
{
    NSTimeInterval secondsInWeek = 7 * 24 * 60 * 60;
    NSDate *nextWeek = [NSDate dateWithTimeInterval:-secondsInWeek sinceDate:date];
    return nextWeek;
}

+ (NSDate *)getDateFrom:(NSDate *)date numDays:(NSInteger)numDays
{
    NSTimeInterval timeInterval = numDays * 24 * 60 * 60;
    NSDate *resultDate = [NSDate dateWithTimeInterval:timeInterval sinceDate:date];
    return resultDate;
}

+ (NSDate *)getEndOfBusinessWeek:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSDateComponents *components =
            [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday
                        fromDate:date];
    [components setWeekday:7];
    [components setWeekOfYear:[components weekOfYear]];

    NSDate *rawDate = [calendar dateFromComponents:components];

    return [Utils getDateOnly:rawDate];
}

+ (NSInteger)dateDifference:(NSDate *)firstDate secondDate:(NSDate *)secondDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = (NSCalendarUnitDay);

    NSDateComponents *components = [calendar components:unitFlags fromDate:firstDate toDate:secondDate options:0];

    NSInteger delta = [components day];

    return labs(delta);
}

+ (NSInteger)hourDifference:(NSDate *)firstDate secondDate:(NSDate *)secondDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = (NSCalendarUnitHour);

    NSDateComponents *components = [calendar components:unitFlags fromDate:firstDate toDate:secondDate options:0];

    NSInteger delta = [components hour];

    return labs(delta);
}

+ (NSInteger)minuteDifference:(NSDate *)firstDate secondDate:(NSDate *)secondDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = (NSCalendarUnitHour);

    NSDateComponents *components = [calendar components:unitFlags fromDate:firstDate toDate:secondDate options:0];

    NSInteger delta = [components minute];

    return labs(delta);
}

+ (NSDate *)convertToUTCDate:(NSDate *)date
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate:date];
    return [NSDate dateWithTimeInterval:seconds sinceDate:date];
}

+ (NSDate *)convertStringToNSDate:(NSString *)dateStr
{
    return [self convertStringToNSDate:dateStr withFormat:kFormatDateUS];
}

+ (NSDate *)convertDateTimeToNSDate:(NSString *)dateTime
{
    NSDate *date = nil;

    if (dateTime) {
        NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT];
        NSInteger startPosition = [dateTime rangeOfString:@"("].location + 1;
        NSTimeInterval unixTime = [[dateTime substringWithRange:NSMakeRange(startPosition, 13)] doubleValue] / 1000;
        date = [[NSDate dateWithTimeIntervalSince1970:unixTime] dateByAddingTimeInterval:offset];
    }
    return date;
}

+ (id)convertNSDateToDateTime:(NSDate *)date
{
    if (!date)
        return [NSNull new];

    NSDateFormatter *formatter = [[DateFormatter sharedFactory] dateFormatterWithFormat:@"Z"];
    NSString *dateTime = [NSString stringWithFormat:@"/Date(%.0f000%@)/", [date timeIntervalSince1970],
                                                    [formatter stringFromDate:date]];
    return dateTime;
}

+ (NSString *)convertNSDateToString:(NSDate *)date
{
    return [self convertNSDateToString:date withFormat:kFormatDateUS];
}

+ (NSString *)convertNSDateToUTCString:(NSDate *)date
{
    return [self convertNSDateToString:date withFormat:kFormatDateUTC];
}

+ (NSString *)convertNSDatetoISO8601String:(NSDate *)date
{
    return [self convertNSDateToString:date withFormat:kFormatDateISO8601];
}

+ (NSString *)convertNSDateToGMTString:(NSDate *)date
{
    return [self convertNSDateToString:date withFormat:kFormatDateGMT];
}

+ (NSString *)convertNSDateToString:(NSDate *)date withFormat:(NSString *)format
{
    if (!date) {
        return kEmptyString;
    }
    NSDateFormatter *dateFormatter =
            [[DateFormatter sharedFactory] dateFormatterWithFormat:format andLocale:[NSLocale currentLocale]];
    return [dateFormatter stringFromDate:date];
}

+ (NSDate *)convertUTCStringToNSDate:(NSString *)dateStr
{
    return [self convertStringToNSDate:dateStr withFormat:kFormatDateUTC];
}

+ (NSDate *)convertISO8601StringtoNSDate:(NSString *)dateStr
{
    return [self convertStringToNSDate:dateStr withFormat:kFormatDateISO8601];
}

+ (NSDate *)convertGMTStringToNSDate:(NSString *)dateStr
{
    return [self convertStringToNSDate:dateStr withFormat:kFormatDateGMT];
}

+ (NSDate *)convertStringToNSDate:(NSString *)dateStr withFormat:(NSString *)format
{
    if (![Utils stringIsNullOrEmpty:dateStr]) {
        NSDateFormatter *dateFormatter = [[DateFormatter sharedFactory] dateFormatterWithFormat:format andLocale:[NSLocale currentLocale]];
        return [dateFormatter dateFromString:dateStr];
    } else {
        return nil;
    }
}

+ (NSDate *)convertDotNetDateToNSDate:(NSString *)dotnetDate
{
    if (dotnetDate == nil)
        return nil;

    if ([dotnetDate isMemberOfClass:[NSNull class]])
        return nil;

    //Get date from inside of braces
    NSRange dataStart = [dotnetDate rangeOfString:@"("];
    NSRange dataEnd = [dotnetDate rangeOfString:@")"];

    //In case that start or end doesn't exist let's return nill
    if (dataStart.length < 1 || dataEnd.length < 1)
        return nil;

    //Parse data inside braces
    NSString *content = [dotnetDate substringWithRange:NSMakeRange(dataStart.location + dataStart.length, dataEnd.location - dataStart.location - dataStart.length)];

    //Split by + or - which identifies timezone
    NSArray *components = [content componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"+-"]];

    //First position are always mills from 1970
    double millsFrom1970 = [components[0] doubleValue];
    double secsFrom1970 = millsFrom1970 / 1000.f;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:secsFrom1970];
    return date;
}

+ (NSString *)convertNSDateToDotNetDate:(NSDate *)date
{
    double timeSince1970 = [date timeIntervalSince1970];
    NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT];
    offset = offset / 3600;
    double nowMillis = 1000.0 * (timeSince1970);
    NSString *dotNetDate = [NSString stringWithFormat:@"/Date(%.0f%+03ld00)/", nowMillis, (long) offset];
    return dotNetDate;
}

///--------------------------------------
#pragma mark - Validation
///--------------------------------------

+ (BOOL)validateAlpha:(NSString *)candidate
{
    return [self validateStringInCharacterSet:candidate characterSet:[NSCharacterSet letterCharacterSet]];
}

+ (BOOL)validateAlphaSpaces:(NSString *)candidate
{
    NSMutableCharacterSet *characterSet = [NSMutableCharacterSet letterCharacterSet];
    [characterSet addCharactersInString:@" "];
    return [self validateStringInCharacterSet:candidate characterSet:characterSet];
}

+ (BOOL)validateAlphanumeric:(NSString *)candidate
{
    return [self validateStringInCharacterSet:candidate characterSet:[NSCharacterSet alphanumericCharacterSet]];
}

+ (BOOL)validateAlphanumericDash:(NSString *)candidate
{
    NSMutableCharacterSet *characterSet = [NSMutableCharacterSet alphanumericCharacterSet];
    [characterSet addCharactersInString:@"-_."];
    return [self validateStringInCharacterSet:candidate characterSet:characterSet];
}

+ (BOOL)validateName:(NSString *)candidate
{
    NSMutableCharacterSet *characterSet = [NSMutableCharacterSet alphanumericCharacterSet];
    [characterSet addCharactersInString:@"'- "];
    return [self validateStringInCharacterSet:candidate characterSet:characterSet];
}

+ (BOOL)validateStringInCharacterSet:(NSString *)string characterSet:(NSCharacterSet *)characterSet
{
    BOOL isEmpty = [self validateNotEmpty:string];
    if (isEmpty)
        return NO;
    else
        return [string rangeOfCharacterFromSet:[characterSet invertedSet]].location == NSNotFound;
}

+ (BOOL)validateNotEmpty:(NSString *)candidate
{
    return !(candidate == nil || [candidate isKindOfClass:[NSNull class]] || [candidate length] == 0);
}

+ (BOOL)validateEmail:(NSString *)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL)validatePhoneNumber:(NSString *)candidate
{
    NSString *regex = @"(\\+[0-9]+[\\- \\.]*)?"         // +<digits><sdd>*
                      @"(\\([0-9]+\\)[\\- \\.]*)?"                // (<digits>)<sdd>*
                      @"([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])";   // <digit><digit|sdd>+<digit>

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:candidate];
}

///--------------------------------------
#pragma mark - External
///--------------------------------------

+ (void)openExternalNavigationFromLocation:(CLLocationCoordinate2D)fromLocation
                                toLocation:(CLLocationCoordinate2D)toLocation
{
    // We will try to open the navigation with Google Maps
    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    if ([APPLICATION.systemApplication canOpenURL:testURL]) {
        NSString *directionsRequest =
                [NSString stringWithFormat:@"comgooglemaps-x-callback://?f=d&daddr=%f,%f&x-success=sourceapp://?resume=true&x-source=AirApp",
                                           toLocation.latitude, toLocation.longitude];
        NSURL *directionsURL = [NSURL URLWithString:directionsRequest];
        [APPLICATION.systemApplication openURL:directionsURL options:@{} completionHandler:nil];
    }

        // Otherwise, use the built-in Apple Maps
    else {
        NSLog(@"%s: Can't use Google Maps on device. Use AppleMaps instead", __func__);
        NSString *directionsRequest =
                [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f&dirflg=d",
                                           fromLocation.latitude,
                                           fromLocation.longitude, toLocation.latitude, toLocation.longitude];
        NSURL *directionsURL = [NSURL URLWithString:directionsRequest];
        [APPLICATION.systemApplication openURL:directionsURL options:@{} completionHandler:nil];
    }
}

+ (void)call:(NSString *)phoneNumber
{
    if ([self validatePhoneNumber:phoneNumber]) {
        // Get the phone URL
        NSString *phoneUrl = [NSString stringWithFormat:@"tel:%@", [self getUnformattedPhoneNumber:phoneNumber]];
        NSURL *url = [NSURL URLWithString:phoneUrl];

        // Open the phone application
        if ([APPLICATION.systemApplication canOpenURL:url]) {
            [APPLICATION.systemApplication openURL:url options:@{} completionHandler:nil];
        } else {
            fTRACE("%@: Phone Application can not be opened", @"Error");
        }
    } else {
        fTRACE("Phone number: <%@> is not valid", phoneNumber);
    }
}

+ (void)text:(NSString *)phoneNumber
{
    if ([self validatePhoneNumber:phoneNumber]) {
        // Get the phone URL
        NSString *phoneUrl = [NSString stringWithFormat:@"sms:%@", [self getUnformattedPhoneNumber:phoneNumber]];
        NSURL *url = [NSURL URLWithString:phoneUrl];

        // Open the sms application
        if ([APPLICATION.systemApplication canOpenURL:url]) {
            [APPLICATION.systemApplication openURL:url options:@{} completionHandler:nil];
        } else {
            fTRACE("%@: Text Application can not be opened", @"Error");
        }
    } else {
        fTRACE("Phone number: <%@> is not valid", phoneNumber);
    }
}

+ (void)email:(NSString *)address subject:(NSString *)subject body:(NSString *)body
{
    if ([self validateEmail:address]) {
        // Get the email URL
        NSString *emailURL = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", address, subject, body];
        NSURL *url = [NSURL URLWithString:emailURL];

        // Open the sms application
        if ([APPLICATION.systemApplication canOpenURL:url]) {
            [APPLICATION.systemApplication openURL:url options:@{} completionHandler:nil];
        } else {
            fTRACE("%@: Email Application can not be opened", @"Error");
        }
    } else {
        fTRACE("Email Address: <%@> is not valid", address);
    }
}

+ (void)showLocalNotification:(nullable NSString *)notificationBody withDate:(nullable NSDate *)notificationDate;
{
    UIApplication *app = [UIApplication sharedApplication];
    UILocalNotification *notification = [[UILocalNotification alloc] init];

    notification.fireDate = notificationDate;
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.alertBody = notificationBody;
    notification.soundName = UILocalNotificationDefaultSoundName;

    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    [notification setUserInfo:userInfo];
    [app scheduleLocalNotification:notification];
}

@end
