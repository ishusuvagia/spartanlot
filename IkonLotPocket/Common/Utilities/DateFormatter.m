
#import "DateFormatter.h"

#ifndef DATEFORMATTERFACTORY_CACHE_LIMIT
#define DATEFORMATTERFACTORY_CACHE_LIMIT 15
#endif

@interface DateFormatter ()

@property(nonatomic, strong) NSCache *_loadedDataFormatters;

@end

@implementation DateFormatter

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)sharedFactory
{
    static DateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [DateFormatter new];
    });
    return dateFormatter;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    self = [super init];
    if (self) {
        __loadedDataFormatters = [NSCache new];
        __loadedDataFormatters.countLimit = DATEFORMATTERFACTORY_CACHE_LIMIT;
    }
    return self;
}

///--------------------------------------
#pragma mark - Factory Methods
///--------------------------------------

- (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format andLocale:(NSLocale *)locale
{
    @synchronized (self) {
        NSString *key = [NSString stringWithFormat:@"%@|%@", format, locale.localeIdentifier];
        NSDateFormatter *dateFormatter = [self._loadedDataFormatters objectForKey:key];
        if (!dateFormatter) {
            dateFormatter = [NSDateFormatter new];
            dateFormatter.dateFormat = format;
            dateFormatter.locale = locale;
            [self._loadedDataFormatters setObject:dateFormatter forKey:key];
        }
        return dateFormatter;
    }
}

- (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format andLocaleIdentifier:(NSString *)localeIdentifier
{
    return [self dateFormatterWithFormat:format andLocale:[[NSLocale alloc] initWithLocaleIdentifier:localeIdentifier]];
}

- (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format
{
    return [self dateFormatterWithFormat:format andLocale:[NSLocale currentLocale]];
}

- (NSDateFormatter *)dateFormatterWithDateStyle:(NSDateFormatterStyle)dateStyle
                                      timeStyle:(NSDateFormatterStyle)timeStyle
                                      andLocale:(NSLocale *)locale
{
    @synchronized (self) {
        NSString *key =
                [NSString stringWithFormat:@"d%lu|t%lu%@", (unsigned long) dateStyle, (unsigned long) timeStyle, locale.localeIdentifier];
        NSDateFormatter *dateFormatter = [self._loadedDataFormatters objectForKey:key];
        if (!dateFormatter) {
            dateFormatter = [NSDateFormatter new];
            dateFormatter.dateStyle = dateStyle;
            dateFormatter.timeStyle = timeStyle;
            dateFormatter.locale = locale;
            [self._loadedDataFormatters setObject:dateFormatter forKey:key];
        }
        return dateFormatter;
    }
}

- (NSDateFormatter *)dateFormatterWithDateStyle:(NSDateFormatterStyle)dateStyle
                                      timeStyle:(NSDateFormatterStyle)timeStyle
                            andLocaleIdentifier:(NSString *)localeIdentifier
{
    return [self dateFormatterWithDateStyle:dateStyle
                                  timeStyle:timeStyle
                                  andLocale:[[NSLocale alloc] initWithLocaleIdentifier:localeIdentifier]];
}

- (NSDateFormatter *)dateFormatterWithDateStyle:(NSDateFormatterStyle)dateStyle andTimeStyle:(NSDateFormatterStyle)timeStyle
{
    return [self dateFormatterWithDateStyle:dateStyle timeStyle:timeStyle andLocale:[NSLocale currentLocale]];
}

@end
