
#ifndef CommonAssert_h
#define CommonAssert_h

#define ParameterAssert(condition, description, ...) \
    do { \
        if (!(condition)) { \
            [NSException raise:NSInvalidArgumentException \
                        format:description, ##__VA_ARGS__]; \
        } \
    } while(0)

#define RangeAssert(condtion, description, ...) \
    do { \
        if (!(condition)) { \
            [NSException raise:NSRangeException \
                        format:description, ##__VA_ARGS__]; \
        } \
    } while(0)

#define ConsistencyAssert(condition, description, ...) \
    do { \
        if (!(condition)) { \
            [NSException raise:NSInternalInconsistencyException \
                        format:description, ##__VA_ARGS__]; \
        } \
    } while(0)

#define NotDesignatedInitializer() \
    do { \
        ConsistencyAssert(NO, \
                          @"%@ is not the designated initializer for the instances of %@.", \
                          NSStringFromSelector(_cmd), \
                          NSStringFromClass([self class])); \
        return nil; \
    } while (0)

#define AssertMainThread() \
    do { \
        ConsistencyAssert([NSThread isMainThread], @"This method must be called on the main thread."); \
    } while (0)

#define AssertIsOnThread(thread) \
    do { \
        ConsistencyAssert([NSThread currentThread] == thread, \
        @"This method must be called on thread: %@", thread); \
    } while (0)

#endif /* CommonAssert_h */
