
#import "UIImage+Bundle.h"

@implementation UIImage (Bundle)

+ (UIImage *)imageInBundleNamed:(NSString *)name
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)])
    {
        // return image in the bundle in current framework
        return [UIImage imageNamed:name inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
    }
    else
#endif
    {
        return [UIImage imageNamed:name];
    }
}

@end
