
#import "AppFont.h"

static CGFloat const kNavigationBarTitleFontSize = 17.0f;
static CGFloat const kLargeNavigationBarTitleFontSize = 34.0f;
static CGFloat const kBarDefaultFontSize = 12.0f;
static CGFloat const kSegmentDefaultFontSize = 13.0f;
static CGFloat const kLabelDefaultFontSize = 15.0f;
static CGFloat const kLabelIndexFontSize = 12.0f;
static CGFloat const kTextFieldDefaultFontSize = 14.0f;
static CGFloat const kTextViewDefaultFontSize = 14.0f;
static CGFloat const kButtonDefaultFontSize = 15.0f;
static CGFloat const kSmallButtonDefaultFontSize = 12.0f;
static CGFloat const kToolBarDefaultFontSize = 15.0f;
static CGFloat const kCalendarTitleFontSize = 13.5f;
static CGFloat const kCalendarSubTitleFontSize = 10.0f;
static CGFloat const kCalendarWeekdayFontSize = 14.0f;
static CGFloat const kCalendarHeaderFontSize = 16.0f;

static NSString *const __nonnull kBoldFont = @"Avenir-Heavy";
static NSString *const __nonnull kBoldObliqueFont = @"Avenir-HeavyOblique";
static NSString *const __nonnull kMediumFont = @"Avenir-Medium";
static NSString *const __nonnull kMediumObliquefont = @"Avenir-MediumOblique";
static NSString *const __nonnull kRegularFont = @"Avenir-Book";
static NSString *const __nonnull kRegularObliqueFont = @"Avenir-BookOblique";
static NSString *const __nonnull kLightFont = @"Avenir-Light";
static NSString *const __nonnull kLightObliqueFont = @"Avenir-LightOblique";

@implementation AppFont

+ (UIFont *)dashboardTileFont
{
    return [UIFont fontWithName:kBoldFont size:kLabelDefaultFontSize];
}

+ (UIFont *)statusBarTitleFont
{
    return [UIFont fontWithName:kBoldFont size:kBarDefaultFontSize];
}

+ (UIFont *)navigationBarTitleFont
{
    return [UIFont fontWithName:kBoldFont size:kNavigationBarTitleFontSize];
}

+ (UIFont *)navigationBarSubtitleFont
{
    return [UIFont fontWithName:kMediumFont size:kLabelIndexFontSize];
}

+ (UIFont *)largeNavigationBarTitleFont
{
    return [UIFont fontWithName:kBoldFont size:kLargeNavigationBarTitleFontSize];
}

+ (UIFont *)tabbarTitleFont
{
    return [UIFont fontWithName:kMediumFont size:kBarDefaultFontSize];
}

+ (UIFont *)tabbarSelectedTitleFont
{
    return [UIFont fontWithName:kBoldFont size:kBarDefaultFontSize];
}

+ (UIFont *)toolBarTitleFont
{
    return [UIFont fontWithName:kMediumFont size:kToolBarDefaultFontSize];
}

+ (UIFont *)segmentTitleFont
{
    return [UIFont fontWithName:kRegularFont size:kSegmentDefaultFontSize];
}

+ (UIFont *)tableHeaderFooterFont
{
    return [UIFont fontWithName:kBoldFont size:kTextViewDefaultFontSize];
}

+ (UIFont *)buttonTitleFont
{
    return [UIFont fontWithName:kBoldFont size:kButtonDefaultFontSize];
}

+ (UIFont *)smallButtonTitleFont
{
    return [UIFont fontWithName:kBoldFont size:kSmallButtonDefaultFontSize];
}

+ (UIFont *)textFieldFont
{
    return [UIFont fontWithName:kMediumFont size:kTextFieldDefaultFontSize];
}

+ (UIFont *)textViewFont
{
    return [UIFont fontWithName:kMediumFont size:kTextViewDefaultFontSize];
}

+ (UIFont *)alertTitleFont
{
    return [UIFont fontWithName:kBoldFont size:kNavigationBarTitleFontSize];
}

+ (UIFont *)alertMessageFont
{
    return [UIFont fontWithName:kRegularFont size:kLabelDefaultFontSize];
}

+ (UIFont *)titleLabelFont
{
    return [UIFont fontWithName:kMediumFont size:kLabelDefaultFontSize];
}

+ (UIFont *)labelFont
{
    return [UIFont fontWithName:kRegularFont size:kLabelDefaultFontSize];
}

+ (UIFont *)statusFont
{
    return [UIFont fontWithName:kMediumFont size:kLabelIndexFontSize];
}

+ (UIFont *)calendarTitleFont
{
    return [UIFont fontWithName:kMediumFont size:kCalendarTitleFontSize];
}

+ (UIFont *)calendarSubtitleFont
{
    return [UIFont fontWithName:kRegularFont size:kCalendarSubTitleFontSize];
}

+ (UIFont *)calendarWeekdayFont
{
    return [UIFont fontWithName:kRegularFont size:kCalendarWeekdayFontSize];
}

+ (UIFont *)calendarHeaderFont
{
    return [UIFont fontWithName:kMediumFont size:kCalendarHeaderFontSize];
}

+ (UIFont *)boldFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:kBoldFont size:size];
}

@end
