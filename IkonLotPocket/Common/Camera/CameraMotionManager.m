
#import "CameraMotionManager.h"
#import <CoreMotion/CoreMotion.h>

@interface CameraMotionManager ()
{
    CMAccelerometerHandler _motionHandler;
}

@property (nonatomic, strong) CMMotionManager *_motionManager;
@property (nonatomic, assign) UIDeviceOrientation _lastOrientation;

@end

@implementation CameraMotionManager

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)sharedManager
{
    static CameraMotionManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [CameraMotionManager new];
    });
    return manager;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
        if ([self._motionManager isAccelerometerAvailable]) {
            [self._motionManager setAccelerometerUpdateInterval:.2f];
        } else {
            [self _deviceOrientationDidChangeTo:UIDeviceOrientationLandscapeRight];
        }
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (CMMotionManager *)_motionManager
{
    if (!__motionManager) {
        __motionManager = [CMMotionManager new];
    }
    return __motionManager;
}

- (void)_deviceOrientationDidChangeTo:(UIDeviceOrientation)orientation
{
    [self set_lastOrientation:orientation];

    if (self.motionRotationHandler) {
        self.motionRotationHandler(self._lastOrientation);
    }
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)startMotionHandler
{
    __weak typeof(self) weakSelf = self;
    _motionHandler = ^(CMAccelerometerData *accelerometerData, NSError *error) {
        typeof(self) selfBlock = weakSelf;

        CGFloat xx = (CGFloat) accelerometerData.acceleration.x;
        CGFloat yy = (CGFloat) -accelerometerData.acceleration.y;
        CGFloat zz = (CGFloat) accelerometerData.acceleration.z;

        CGFloat device_angle = (CGFloat) (M_PI / 2.0f - atan2(yy, xx));
        UIDeviceOrientation orientation = UIDeviceOrientationUnknown;

        if (device_angle > M_PI)
            device_angle -= 2 * M_PI;

        if ((zz < -.60f) || (zz > .60f)) {
            if (UIDeviceOrientationIsLandscape(selfBlock._lastOrientation))
                orientation = selfBlock._lastOrientation;
            else
                orientation = UIDeviceOrientationUnknown;
        } else {
            if ((device_angle > -M_PI_4) && (device_angle < M_PI_4))
                orientation = UIDeviceOrientationPortrait;
            else if ((device_angle < -M_PI_4) && (device_angle > -3 * M_PI_4))
                orientation = UIDeviceOrientationLandscapeLeft;
            else if ((device_angle > M_PI_4) && (device_angle < 3 * M_PI_4))
                orientation = UIDeviceOrientationLandscapeRight;
            else
                orientation = UIDeviceOrientationPortraitUpsideDown;
        }

        if (orientation != selfBlock._lastOrientation) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [selfBlock _deviceOrientationDidChangeTo:orientation];
            });
        }
    };

    [self._motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:_motionHandler];
}

@end
