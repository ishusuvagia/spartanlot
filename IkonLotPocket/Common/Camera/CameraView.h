
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CameraViewDelegate <NSObject>

@optional
- (void)cameraView:(nullable UIView *)camera focusAtPoint:(CGPoint)point;
- (void)cameraView:(nullable UIView *)camera exposeAtPoint:(CGPoint)point;
- (void)cameraViewStartRecording;
- (void)closeCamera;
- (void)switchCamera;
- (void)triggerFlashForMode:(AVCaptureFlashMode)flashMode;
- (void)cameraView:(nullable UIView *)camera showGridView:(BOOL)show;
- (void)openLibrary;
- (BOOL)cameraViewHasFocus;
- (void)cameraCaptureScale:(CGFloat)scaleNum;
- (CGFloat)cameraMaxScale;
@end

@protocol CameraViewSettings <NSObject>

@property (nullable, nonatomic, strong) UIColor *tintColor;
@property (nullable, nonatomic, strong) UIColor *selectedTintColor;

@end

@interface CameraView : UIView <UIGestureRecognizerDelegate, CameraViewSettings>

@property (nullable, nonatomic, weak) id <CameraViewDelegate> delegate;
@property (nullable, nonatomic, strong) CALayer *focusBox;
@property (nullable, nonatomic, strong) CALayer *exposeBox;
@property (nullable, nonatomic, strong) UIButton *photoLibraryButton;
@property (nullable, nonatomic, strong) UIButton *triggerButton;
@property (nullable, nonatomic, strong) UIButton *flashButton;
@property (nullable, nonatomic, strong) UIButton *gridButton;
@property (nullable, nonatomic, strong) UIButton *cameraButton;
@property (nullable, nonatomic, strong, readonly) AVCaptureVideoPreviewLayer *previewLayer;
@property (nullable, nonatomic, strong, readonly) UITapGestureRecognizer *singleTap;
@property (nullable, nonatomic, strong, readonly) UITapGestureRecognizer *doubleTap;
@property (nullable, nonatomic, strong, readonly) UIPanGestureRecognizer *panGestureRecognizer;
@property (nullable, nonatomic, strong) UIPinchGestureRecognizer *pinch;

+ (instancetype)initWithFrame:(CGRect)frame;
+ (nonnull CameraView *)initWithCaptureSession:(nullable AVCaptureSession *)captureSession;
- (void)defaultInterface;
- (void)draw:(nullable CALayer *)layer atPointOfInterest:(CGPoint)point andRemove:(BOOL)remove;
- (void)drawFocusBoxAtPointOfInterest:(CGPoint)point andRemove:(BOOL)remove;
- (void)drawExposeBoxAtPointOfInterest:(CGPoint)point andRemove:(BOOL)remove;
- (void)pinchCameraViewWithScaleNum:(CGFloat)scale;
- (void)enableActionButtons;

@end

NS_ASSUME_NONNULL_END
