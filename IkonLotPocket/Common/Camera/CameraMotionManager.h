
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^MotionManagerRotationHandler)(UIDeviceOrientation);

@interface CameraMotionManager : NSObject

@property (nonatomic, copy) MotionManagerRotationHandler motionRotationHandler;

+ (instancetype)sharedManager;
- (void)startMotionHandler;

@end

NS_ASSUME_NONNULL_END
