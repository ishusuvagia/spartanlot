
#import <SIAlertView/SIAlertView.h>
#import "ThemeManager.h"
#import "AppColorScheme.h"
#import "AppColor.h"
#import "AppFont.h"

@implementation ThemeManager

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (void)setDefaultTheme
{
    [self setNavigationBarDefaultTheme];
    [self _setTabBarTheme];
    [self _setSearchBarTheme];
    [self _setToolbarTheme];
    [self _setPageControlTheme];
    [self _setSegmentedControlTheme];
    [self _setTableViewTheme];
    [self _setTableViewCellTheme];
    [self _setPickerViewTheme];
    [self _setButtonTheme];
    [self _setTextFieldTheme];
    [self _setTextViewTheme];
    [self _setPickerTheme];
    [self _setAlertTheme];
}

+ (void)setNavigationBarDefaultTheme
{
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:nil];
    [[UINavigationBar appearance] setBarTintColor:[AppColor navigationBarBackgroundColor]];
    [[UINavigationBar appearance] setTintColor:[AppColor navigationBarTintColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    [[UINavigationBar appearance] setHidden:YES];

    [[UINavigationBar appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor navigationBarTextColor],
            NSFontAttributeName: [AppFont navigationBarTitleFont]
    }];
}

+ (void)setNavigationBarTransparentTheme
{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    [[UINavigationBar appearance] setTintColor:[AppColor navigationBarTintColor]];
    [[UINavigationBar appearance] setTranslucent:YES];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    [[UINavigationBar appearance] setHidden:YES];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor navigationBarTextColor],
            NSFontAttributeName: [AppFont navigationBarTitleFont]
    }];
}

///--------------------------------------
#pragma mark - Private Class Helper
///--------------------------------------

+ (void)_setTabBarTheme
{
    // [UITabBar appearance].barTintColor = [AppColor tabBarBackgroundColor];
    [UITabBar appearance].tintColor = [AppColor tabBarTintColor];
    [UITabBarItem appearance].titlePositionAdjustment = UIOffsetMake(0, -2);

    [[UITabBarItem appearance] setTitleTextAttributes:@
            {NSForegroundColorAttributeName: [AppColor tabBarItemUnSelectedTitleColor],
                    NSFontAttributeName: [AppFont tabbarTitleFont]
            }
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                    NSForegroundColorAttributeName: [AppColor tabBarItemSelectedTitleColor],
                    NSFontAttributeName: [AppFont tabbarTitleFont]
            }
                                             forState:UIControlStateSelected];
}

+ (void)_setSearchBarTheme
{
    [[UISearchBar appearance] setBackgroundColor:[AppColor searchBarBackgroundColor]];
    [[UISearchBar appearance] setBarTintColor:[AppColor searchBarBarTintColor]];
    [[UISearchBar appearance] setTintColor:[AppColor searchBarTintColor]];
    [[UISearchBar appearance] setTranslucent:YES];
    [[UISearchBar appearance] setSearchBarStyle:UISearchBarStyleDefault];
    [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"img-toolbar-background"]];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor toolBarTextColor],
            NSFontAttributeName: [AppFont toolBarTitleFont]
    }                                           forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor toolBarDisabledColor],
            NSFontAttributeName: [AppFont toolBarTitleFont]
    }                                           forState:UIControlStateDisabled];
}

+ (void)_setToolbarTheme
{
    [UIToolbar appearance].backgroundColor = [AppColor toolBarBackgroundColor];
    [UIToolbar appearance].barTintColor = [AppColor toolBarBarTintColor];
    [UIToolbar appearance].tintColor = [AppColor toolBarTintColor];
    [UIToolbar appearance].translucent = NO;

    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor toolBarTextColor],
            NSFontAttributeName: [AppFont toolBarTitleFont]
    }                                           forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor toolBarDisabledColor],
            NSFontAttributeName: [AppFont toolBarTitleFont]
    }                                           forState:UIControlStateDisabled];
}

+ (void)_setPageControlTheme
{
    [UIPageControl appearance].pageIndicatorTintColor = [AppColor pageControlIndicatorTintColor];
    [UIPageControl appearance].currentPageIndicatorTintColor = [AppColor pageControlCurrentIndicatorTintColor];
    [UIPageControl appearance].backgroundColor = [AppColor pageControlBackgroundColor];
}

+ (void)_setSegmentedControlTheme
{
    [UISegmentedControl appearance].tintColor = [AppColor segmentedControlTintColor];
    [UISegmentedControl appearance].backgroundColor = [AppColor segmentedControlBackgroundColor];

    [[UISegmentedControl appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor segmentedUnselectedColor],
            NSFontAttributeName: [AppFont segmentTitleFont]
    }                                              forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor segmentedControlTintColor],
            NSFontAttributeName: [AppFont segmentTitleFont]
    }                                              forState:UIControlStateHighlighted];
}

+ (void)_setTableViewTheme
{
    [UITableView appearance].sectionIndexColor = [AppColor tableViewHeaderForegroundColor];
    [UITableView appearance].sectionIndexBackgroundColor = [AppColor tableViewHeaderBackgroundColor];
    [UITableView appearance].sectionIndexTrackingBackgroundColor = [AppColor tableViewHeaderBackgroundColor];
    [UITableView appearance].backgroundColor = [AppColor tableViewBackgroundColor];
    [UITableView appearance].separatorColor = [AppColor tableViewSeparatorColor];
    [UITableView appearance].separatorInset = UIEdgeInsetsZero;
    [UITableView appearance].layoutMargins = UIEdgeInsetsZero;
    [UITableView appearance].separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [UITableView appearance].indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [UITableView appearance].showsHorizontalScrollIndicator = NO;
    [UITableView appearance].showsVerticalScrollIndicator = NO;
    [UITableView appearance].tintColor = [AppColor tableViewTintColor];
    [UITableViewHeaderFooterView appearance].tintColor = [AppColor tableViewTintColor];
}

+ (void)_setTableViewCellTheme
{
    UIView *selectedBackgroundView = [UIView new];
    [selectedBackgroundView setBackgroundColor:[AppColor tableViewSelectedCellBackgroundColor]];

    [UITableViewCell appearance].backgroundColor = [AppColor tableViewCellBackgroundColor];
    [UITableViewCell appearance].selectedBackgroundView = selectedBackgroundView;
    [UITableViewCell appearance].layoutMargins = UIEdgeInsetsZero;
    [UITableViewCell appearance].selectedBackgroundView = selectedBackgroundView;
}

+ (void)_setPickerViewTheme
{
    [UIPickerView appearance].backgroundColor = [AppColor pickerViewBackgroundColor];
    [UIPickerView appearance].tintColor = [AppColor pickerViewTintColor];
    [UIDatePicker appearance].backgroundColor = [AppColor pickerViewBackgroundColor];
    [UIDatePicker appearance].tintColor = [AppColor pickerViewTintColor];
}

+ (void)_setButtonTheme
{
    [UIButton appearance].layer.borderColor = [AppColor buttonDisabledTintColor].CGColor;
    [[UIButton appearance].titleLabel setFont:[AppFont buttonTitleFont]];
}

+ (void)_setTextFieldTheme
{
    [UITextField appearance].tintColor = [AppColor textFieldTintColor];
    [UITextField appearance].backgroundColor = [AppColor textFieldBackgroundColor];
    [UITextField appearance].font = [AppFont textFieldFont];
}

+ (void)_setTextViewTheme
{
    [UITextView appearance].tintColor = [AppColor textViewTintColor];
    [UITextView appearance].textColor = [AppColor textViewTextColor];
    [UITextView appearance].backgroundColor = [AppColor textViewBackgroundColor];
    [UITextView appearance].font = [AppFont textViewFont];
}

+ (void)_setPickerTheme
{
    [UIPickerView appearance].backgroundColor = [AppColor pickerViewBackgroundColor];
    [UIPickerView appearance].tintColor = [AppColor pickerViewTintColor];
    [UIDatePicker appearance].backgroundColor = [AppColor pickerViewBackgroundColor];
    [UIDatePicker appearance].tintColor = [AppColor pickerViewTintColor];
}

+ (void)_setAlertTheme
{
    [[SIAlertView appearance] setTitleFont:[AppFont alertTitleFont]];
    [[SIAlertView appearance] setMessageFont:[AppFont textFieldFont]];
    [[SIAlertView appearance] setMessageFont:[AppFont textFieldFont]];
    [[SIAlertView appearance] setMessageColor:[AppColor textStyleTitleColor]];
    [[SIAlertView appearance] setCornerRadius:5.0f];
    [[SIAlertView appearance] setShadowRadius:10.0f];
    [[SIAlertView appearance] setTransitionStyle:SIAlertViewTransitionStyleBounce];
    [[SIAlertView appearance] setButtonFont:[AppFont buttonTitleFont]];
    [[SIAlertView appearance] setButtonColor:[AppColorScheme success]];
    [[SIAlertView appearance] setCancelButtonColor:[AppColorScheme blue]];
}

@end
