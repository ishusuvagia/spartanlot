
#import "AppColor.h"
#import "AppColorScheme.h"

@implementation AppColor

#define SCHEME(name) { return [AppColorScheme name]; }

///--------------------------------------
#pragma mark - UINavigationBar
///--------------------------------------

+ (UIColor *)navigationBarBackgroundColor SCHEME(clear)

+ (UIColor *)navigationBarTintColor SCHEME(white)

+ (UIColor *)navigationBarTextColor SCHEME(white)

+ (UIColor *)navigationBarDisabledColor SCHEME(lightGray)

+ (UIColor *)navigationBarPageIndicatorColor SCHEME(white)

+ (UIColor *)navigationBarPageHairLineColor SCHEME(brandLightBlue)

+ (UIColor *)navigationBarSelectedMenuItemLabelColor SCHEME(white);

+ (UIColor *)navigationBarUnselectedMenuItemLabelColor SCHEME(lightGray);

///--------------------------------------
#pragma mark - UITabBar
///--------------------------------------

+ (UIColor *)tabBarBackgroundColor SCHEME(white)

+ (UIColor *)tabBarTintColor SCHEME(blue)

+ (UIColor *)tabBarItemUnSelectedTitleColor SCHEME(lightGray)

+ (UIColor *)tabBarItemSelectedTitleColor SCHEME(blue)

+ (UIColor *)tabBarImageSelectedColor SCHEME(blue)

///--------------------------------------
#pragma mark - UISearchbar
///--------------------------------------

+ (UIColor *)searchBarBackgroundColor SCHEME(brandDarkBlue)

+ (UIColor *)searchBarBarTintColor SCHEME(brandDarkBlue)

+ (UIColor *)searchBarTintColor SCHEME(white)

+ (UIColor *)searchTextColor SCHEME(white)

+ (UIColor *)searchDisabledColor SCHEME(lightGray)

///--------------------------------------
#pragma mark - UIToolbar
///--------------------------------------

+ (UIColor *)toolBarBackgroundColor SCHEME(white)

+ (UIColor *)toolBarBarTintColor SCHEME(brandDarkBlue)

+ (UIColor *)toolBarTintColor SCHEME(white)

+ (UIColor *)toolBarTextColor SCHEME(white)

+ (UIColor *)toolBarDisabledColor SCHEME(lightGray)

///--------------------------------------
#pragma mark - UISegmentedControl
///--------------------------------------

+ (UIColor *)segmentedControlTintColor SCHEME(brandBlue)

+ (UIColor *)segmentedUnselectedColor SCHEME(darkGray)

+ (UIColor *)segmentedControlBackgroundColor SCHEME(clear)

///--------------------------------------
#pragma mark - UITableView
///--------------------------------------

+ (UIColor *)tableViewBackgroundColor SCHEME(clear)

+ (UIColor *)tableViewHeaderBackgroundColor SCHEME(brandDarkBlue)

+ (UIColor *)tableViewHeaderForegroundColor SCHEME(clear)

+ (UIColor *)tableViewTintColor SCHEME(blue)

+ (UIColor *)tableViewGroupedHeaderAndFooterForegroundColor SCHEME(white)

+ (UIColor *)tableViewGroupedHeaderAndFooterBackgroundColor SCHEME(silver)

+ (UIColor *)tableViewCellBackgroundColor SCHEME(clear)

+ (UIColor *)tableViewCellDetailsColor SCHEME(white)

+ (UIColor *)tableViewPrimaryColumnBackgroundColor SCHEME(silver)

+ (UIColor *)tableViewSelectedCellBackgroundColor SCHEME(brandBlue)

+ (UIColor *)tableViewSeparatorColor SCHEME(lightGray)

+ (UIColor *)tableViewEmptyCellBackgroundColor SCHEME(clear)

+ (UIColor *)tableViewSectionIndexColor SCHEME(darkGray)

+ (UIColor *)tableViewSectionIndexBackgroundColor SCHEME(clear)

+ (UIColor *)tableViewSectionIndexTrackingBackgroundColor SCHEME(lightGray)

///--------------------------------------
#pragma mark - UILabel & UITextField
///--------------------------------------

+ (UIColor *)textStyleTitleColor SCHEME(darkGray)

+ (UIColor *)textStyleTitleShadowColor SCHEME(white)

+ (UIColor *)textStyleBodyColor SCHEME(darkGray)

+ (UIColor *)textStyleDescriptionColor SCHEME(lightGray)

+ (UIColor *)textStyleDisabledBodyColor SCHEME(lightGray)

+ (UIColor *)textStylePlaceholderColor SCHEME(lightGray)

+ (UIColor *)textStyleErrorColor SCHEME(red);

+ (UIColor *)textStyleSuccessColor SCHEME(green)

+ (UIColor *)textStyleLinkColor SCHEME(brandBlue)

///--------------------------------------
#pragma mark - UIButton & UIBarButtonItem
///--------------------------------------

+ (UIColor *)buttonDeleteTintColor SCHEME(red)

+ (UIColor *)buttonTintColor SCHEME(white)

+ (UIColor *)buttonDisabledTintColor SCHEME(lightGray)

+ (UIColor *)buttonInfoTextColor SCHEME(blue)

+ (UIColor *)buttonSubmitShadowColor SCHEME(black)

+ (UIColor *)buttonTextColor SCHEME(blue)

///--------------------------------------
#pragma mark - UIPageControl
///--------------------------------------

+ (UIColor *)pageControlIndicatorTintColor SCHEME(lightGray)

+ (UIColor *)pageControlCurrentIndicatorTintColor SCHEME(black)

+ (UIColor *)pageControlBackgroundColor SCHEME(clear)

///--------------------------------------
#pragma mark - UIPickerView
///--------------------------------------

+ (UIColor *)pickerViewBackgroundColor SCHEME(lightWhite)

+ (UIColor *)pickerViewTintColor SCHEME(white)

+ (UIColor *)pickerViewTextColor SCHEME(white)

///--------------------------------------
#pragma mark - UITextField
///--------------------------------------

+ (UIColor *)textFieldTintColor SCHEME(blue);

+ (UIColor *)textFieldBackgroundColor SCHEME(clear);

///--------------------------------------
#pragma mark - UITextView
///--------------------------------------

+ (UIColor *)textViewTintColor SCHEME(blue);

+ (UIColor *)textViewBackgroundColor SCHEME(white);

+ (UIColor *)textViewTextColor SCHEME(brandBlack);

///--------------------------------------
#pragma mark - UITextView
///--------------------------------------

+ (nonnull UIColor *)titleDefaultColor SCHEME(brandBlack)

+ (nonnull UIColor *)headerTitleColor SCHEME(brandBlue)

+ (nonnull UIColor *)weekdayTextColor SCHEME(brandBlue)

+ (nonnull UIColor *)todayColor SCHEME(brandBlue)

+ (nonnull UIColor *)selectionColor SCHEME(brandRed)

+ (nonnull UIColor *)todaySelectionColor SCHEME(brandDarkBlue)

@end
