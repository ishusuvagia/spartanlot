
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppColor : NSObject

///--------------------------------------
/// @name UINavigationBar
///--------------------------------------

+ (nonnull UIColor *)navigationBarBackgroundColor;
+ (nonnull UIColor *)navigationBarTintColor;
+ (nonnull UIColor *)navigationBarTextColor;
+ (nonnull UIColor *)navigationBarDisabledColor;
+ (nonnull UIColor *)navigationBarPageIndicatorColor;
+ (nonnull UIColor *)navigationBarPageHairLineColor;
+ (nonnull UIColor *)navigationBarSelectedMenuItemLabelColor;
+ (nonnull UIColor *)navigationBarUnselectedMenuItemLabelColor;

///--------------------------------------
/// @name UITabBar
///--------------------------------------

+ (nonnull UIColor *)tabBarBackgroundColor;
+ (nonnull UIColor *)tabBarTintColor;
+ (nonnull UIColor *)tabBarItemUnSelectedTitleColor;
+ (nonnull UIColor *)tabBarItemSelectedTitleColor;
+ (nonnull UIColor *)tabBarImageSelectedColor;

///--------------------------------------
/// @name UISearchbar
///--------------------------------------

+ (nonnull UIColor *)searchBarBackgroundColor;
+ (nonnull UIColor *)searchBarBarTintColor;
+ (nonnull UIColor *)searchBarTintColor;
+ (nonnull UIColor *)searchTextColor;
+ (nonnull UIColor *)searchDisabledColor;

///--------------------------------------
/// @name UIToolbar
///--------------------------------------

+ (nonnull UIColor *)toolBarBackgroundColor;
+ (nonnull UIColor *)toolBarBarTintColor;
+ (nonnull UIColor *)toolBarTintColor;
+ (nonnull UIColor *)toolBarTextColor;
+ (nonnull UIColor *)toolBarDisabledColor;

///--------------------------------------
/// @name UISegmentedControl
///--------------------------------------

+ (nonnull UIColor *)segmentedControlTintColor;
+ (nonnull UIColor *)segmentedUnselectedColor;
+ (nonnull UIColor *)segmentedControlBackgroundColor;

///--------------------------------------
/// @name UITableView
///--------------------------------------

+ (nonnull UIColor *)tableViewBackgroundColor;
+ (nonnull UIColor *)tableViewHeaderBackgroundColor;
+ (nonnull UIColor *)tableViewHeaderForegroundColor;
+ (nonnull UIColor *)tableViewGroupedHeaderAndFooterForegroundColor;
+ (nonnull UIColor *)tableViewGroupedHeaderAndFooterBackgroundColor;
+ (nonnull UIColor *)tableViewTintColor;
+ (nonnull UIColor *)tableViewCellBackgroundColor;
+ (nonnull UIColor *)tableViewCellDetailsColor;
+ (nonnull UIColor *)tableViewSelectedCellBackgroundColor;
+ (nonnull UIColor *)tableViewSeparatorColor;
+ (nonnull UIColor *)tableViewEmptyCellBackgroundColor;
+ (nonnull UIColor *)tableViewSectionIndexColor;
+ (nonnull UIColor *)tableViewPrimaryColumnBackgroundColor;
+ (nonnull UIColor *)tableViewSectionIndexBackgroundColor;
+ (nonnull UIColor *)tableViewSectionIndexTrackingBackgroundColor;

///--------------------------------------
/// @name UILabel & UITextField
///--------------------------------------

+ (nonnull UIColor *)textStyleTitleColor;
+ (nonnull UIColor *)textStyleTitleShadowColor;
+ (nonnull UIColor *)textStyleBodyColor;
+ (nonnull UIColor *)textStyleDescriptionColor;
+ (nonnull UIColor *)textStyleDisabledBodyColor;
+ (nonnull UIColor *)textStylePlaceholderColor;
+ (nonnull UIColor *)textStyleErrorColor;
+ (nonnull UIColor *)textStyleSuccessColor;
+ (nonnull UIColor *)textStyleLinkColor;

///--------------------------------------
/// @name UIButton & UIBarButtonItem
///--------------------------------------

+ (nonnull UIColor *)buttonDeleteTintColor;
+ (nonnull UIColor *)buttonTintColor;
+ (nonnull UIColor *)buttonDisabledTintColor;
+ (nonnull UIColor *)buttonInfoTextColor;
+ (nonnull UIColor *)buttonSubmitShadowColor;
+ (nonnull UIColor *)buttonTextColor;

///--------------------------------------
/// @name UIPageControl
///--------------------------------------

+ (nonnull UIColor *)pageControlIndicatorTintColor;
+ (nonnull UIColor *)pageControlCurrentIndicatorTintColor;
+ (nonnull UIColor *)pageControlBackgroundColor;

///--------------------------------------
/// @name UIPickerView
///--------------------------------------

+ (nonnull UIColor *)pickerViewBackgroundColor;
+ (nonnull UIColor *)pickerViewTintColor;
+ (nonnull UIColor *)pickerViewTextColor;

///--------------------------------------
/// @name UITextField
///--------------------------------------

+ (nonnull UIColor *)textFieldTintColor;
+ (nonnull UIColor *)textFieldBackgroundColor;

///--------------------------------------
/// @name UITextView
///--------------------------------------

+ (nonnull UIColor *)textViewTintColor;
+ (nonnull UIColor *)textViewBackgroundColor;
+ (nonnull UIColor *)textViewTextColor;

///--------------------------------------
/// @name Calendar
///--------------------------------------

+ (nonnull UIColor *)titleDefaultColor;
+ (nonnull UIColor *)headerTitleColor;
+ (nonnull UIColor *)weekdayTextColor;
+ (nonnull UIColor *)todayColor;
+ (nonnull UIColor *)selectionColor;
+ (nonnull UIColor *)todaySelectionColor;

@end

NS_ASSUME_NONNULL_END
