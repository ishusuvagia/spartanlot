
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class DatePickerLabel;

@protocol DatePickerLabelProtocol <NSObject>
@optional
- (void)updatewithNewDate:(nullable NSDate *)date ofLabel:(nonnull DatePickerLabel *)datePickerLabel;
- (void)updateWithNewCountDownDuration:(NSTimeInterval)countDownDuration ofLabel:(nonnull DatePickerLabel *)datePickerLabel;

@end

@interface DatePickerLabel : UILabel

@property (nonatomic, assign) BOOL isActionOverridenByContainer;
@property (nonatomic, assign) UIDatePickerMode datePickerMode;
@property (nonnull, nonatomic, strong, readonly) UIView *inputView;
@property (nonnull, nonatomic, strong, readonly) UIView *inputAccessoryView;
@property (nullable, nonatomic, strong) NSString *inputAccessoryPrefix;
@property (nullable, nonatomic, weak) id <DatePickerLabelProtocol> protocol;

@end

NS_ASSUME_NONNULL_END
