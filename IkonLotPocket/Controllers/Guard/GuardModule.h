//
//  GuardModule.h
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GuardModule : NSObject <ModuleDelegate>

@end

NS_ASSUME_NONNULL_END
