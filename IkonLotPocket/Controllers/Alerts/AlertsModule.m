//
//  AlertsModule.m
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "AlertsModule.h"
#import "AlertsViewController.h"

@interface AlertsModule ()

@property (nonatomic, strong) AlertsViewController *_controller;

@end
@implementation AlertsModule
- (NSString *)getTitle
{
    return LOCALIZED(@"title_alerts");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-alert"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-alert"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-alert-selected"];
}

- (UIViewController *)getMainViewController
{
    if (!self._controller) {
        __controller = INIT_CONTROLLER_XIB(AlertsViewController);
    }
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeMap;
}

- (BOOL)alwaysVisible
{
    return NO;
}

@end
