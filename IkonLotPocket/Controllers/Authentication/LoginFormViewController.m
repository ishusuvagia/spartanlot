
#import "LoginFormViewController.h"

@interface LoginFormViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *_usernameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *_passwordTxtField;
@property (weak,nonatomic) IBOutlet UITableView *LoginTable;

@end

@implementation LoginFormViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];

    self._usernameTxtField.text = kEmptyString;
    self._passwordTxtField.text = kEmptyString;
//    self._usernameTxtField.placeholder = LOCALIZED(@"username");
//    self._passwordTxtField.placeholder = LOCALIZED(@"password");
    NSString *previousUsername = [Utils getObjectFromUserDefaults:kPrefUsernameKey];
    if (previousUsername) self._usernameTxtField.text = previousUsername;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(30,18, 16, 18)];
    imgView.image = [UIImage imageNamed:@"login-user"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [paddingView addSubview:imgView];
    [self._usernameTxtField setLeftViewMode:UITextFieldViewModeAlways];
    [self._usernameTxtField setLeftView:paddingView];
    
    UIImageView *imgViewps = [[UIImageView alloc] initWithFrame:CGRectMake(30,17, 16, 20)];
    imgViewps.image = [UIImage imageNamed:@"login-password"];
    UIView *paddingViewps = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [paddingViewps addSubview:imgViewps];
    [self._passwordTxtField setLeftViewMode:UITextFieldViewModeAlways];
    [self._passwordTxtField setLeftView:paddingViewps];
    
    self._usernameTxtField.delegate = self;
    self._passwordTxtField.delegate = self;
    
    self.tableView.layer.borderWidth = 0.2f;
    self.tableView.layer.borderColor = [UIColor clearColor].CGColor;
    self.tableView.backgroundColor = [UIColor clearColor];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)dealloc
{
    TRACE_HERE;
    self._usernameTxtField = nil;
    self._passwordTxtField = nil;
}

///--------------------------------------
#pragma mark - <UITextFieldDelegate>
///--------------------------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self._usernameTxtField) {
        [self._passwordTxtField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        if ([self.delegate respondsToSelector:@selector(isReadyToExecute)]) {
            [self.delegate isReadyToExecute];
        }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self.delegate respondsToSelector:@selector(didFocus)]) {
        [self.delegate didFocus];
    }
    return YES;
}

///--------------------------------------
#pragma mark - <UITableViewDelegate>
///--------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) [self._usernameTxtField becomeFirstResponder];
    else if (indexPath.row == 1) [self._passwordTxtField becomeFirstResponder];
}

///--------------------------------------
#pragma mark - Accessors
///--------------------------------------

- (NSString *)username
{
    return self._usernameTxtField.text ?: kEmptyString;
}

- (NSString *)password
{
    return self._passwordTxtField.text ?: kEmptyString;
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (void)resignAllTextFields
{
    [self.tableView endEditing:YES];
    [self._usernameTxtField resignFirstResponder];
    [self._passwordTxtField resignFirstResponder];
}

- (void)clearPasswordField
{
    [self resignAllTextFields];
    self._passwordTxtField.text = kEmptyString;
}

- (void)disableForm
{
    [self resignAllTextFields];
    [self._usernameTxtField setEnabled:NO];
    [self._passwordTxtField setEnabled:NO];
}

- (void)enableForm
{
    [self._usernameTxtField setEnabled:YES];
    [self._passwordTxtField setEnabled:YES];
    
    if (self._usernameTxtField.text.length == 0) {
        [self._usernameTxtField becomeFirstResponder];
    } else {
        [self._passwordTxtField becomeFirstResponder];
    }
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)focusOnUsernameTextField
{
    [self._usernameTxtField becomeFirstResponder];
}

- (void)focusOnPasswordTextField
{
    [self._passwordTxtField becomeFirstResponder];
}

@end
