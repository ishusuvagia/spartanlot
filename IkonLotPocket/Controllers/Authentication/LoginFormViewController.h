
#import "FormViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginFormViewController : FormViewController

@property (nonatomic, readonly) NSString *username;
@property (nonatomic, readonly) NSString *password;


- (void)focusOnUsernameTextField;
- (void)focusOnPasswordTextField;

@end

NS_ASSUME_NONNULL_END
