#import "LoginViewController.h"
#import "LoginFormViewController.h"
#import "SectionTableView.h"
#import "CustomActivityButton.h"

@interface LoginViewController () <FormDelegate>
{
    BOOL _isFracnhiseeUser;
}

@property (weak, nonatomic) IBOutlet UIImageView *_logoImgView;
@property (weak, nonatomic) IBOutlet UIView *_formContainerView;
@property (weak, nonatomic) IBOutlet CustomActivityButton *_loginBtn;
@property (weak, nonatomic) IBOutlet CustomButton *_forgotPasswordBtn;
@property (weak, nonatomic) IBOutlet UILabel *_versionLbl;

- (IBAction)_handleLoginBtnEvent:(id)sender;
- (IBAction)_handleForgotPasswordBtnEvent:(id)sender;

@property (strong, nonatomic) LoginFormViewController *_loginFormViewController;

@end

@implementation LoginViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Version Label
    NSString *version = LOCALIZEDFORMAT(@"version %@", APPLICATION.version);
#ifdef DEBUG
    self._versionLbl.text = [NSString stringWithFormat:@"IkonLotPocket %@ - 04/10/2019 - %@", version, APPLICATION.buildMode];
#else
    self._versionLbl.text = version;
#endif
    self._loginBtn.layer.cornerRadius = self._loginBtn.frame.size.height/2;
    self._loginBtn.clipsToBounds=YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self._formContainerView.backgroundColor=[UIColor clearColor];
    [self._loginFormViewController enableForm];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self._loginBtn hideActivity];
    
    [self._loginFormViewController clearPasswordField];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueLoginForm]) {
        __loginFormViewController = segue.destinationViewController;
        self._loginFormViewController.delegate = self;
    }
}

///--------------------------------------
#pragma mark - UI & Events
///--------------------------------------

- (IBAction)_handleLoginBtnEvent:(id)sender
{
    [self._loginFormViewController resignAllTextFields];
    [self _login];
}

- (IBAction)_handleForgotPasswordBtnEvent:(id)sender
{
    // Implement later
}

///--------------------------------------
#pragma mark - Authentication
///--------------------------------------

- (void)_login
{
    NSString *username = self._loginFormViewController.username;
    NSString *password = self._loginFormViewController.password;
    [self _loginWithUsername:username password:password];
}

- (void)_loginWithUsername:(NSString *)username password:(NSString *)password
{
    // Validate Inputs
    if (![Utils validateNotEmpty:username]) {
        [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:LOCALIZED(@"error_usernameisempty") confirm:^{
            [self._loginFormViewController focusOnUsernameTextField];
        }];
    } else if (![Utils validateNotEmpty:password]) {
        [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:LOCALIZED(@"error_passwordisempty") confirm:^{
            [self._loginFormViewController focusOnPasswordTextField];
        }];
    } else {
        [self._loginFormViewController resignAllTextFields];
        [self._loginBtn showActivity];
        [self._loginFormViewController disableForm];
        [AUTHMANAGER loginWithUsername:username password:password callback:^(BOOL success, NSError *error) {
            [self._loginBtn hideActivity];
            [self._loginFormViewController enableForm];
            if (success) {
                if (self.isModal) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    [self performSegueWithIdentifier:kSegueLanding sender:self];
                }
            } else {
                [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:error.localizedDescription confirm:^{
                    [self._loginFormViewController focusOnUsernameTextField];
                }];
            }
        }];
    }
}

///--------------------------------------
#pragma mark - <FormDelegate>
///--------------------------------------

- (void)isReadyToExecute
{
    [self _login];
}

@end
