
#import "BaseViewController.h"

@implementation BaseViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)dealloc
{
    TRACE_HERE;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (BOOL)isVisible
{
    return [self isViewLoaded] && self.view.window;
}

@end
