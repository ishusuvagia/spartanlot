
#import "BaseViewController.h"
#import "BaseViewController+Common.h"
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BarButtonsDelegate <NSObject>
@optional
- (nonnull UIButton *)setupLeftBarButton;
@optional
- (nonnull UIButton *)setupRightBarButton;
@end

@interface NavigationBarViewController : BaseViewController

- (instancetype)initWithBarButtonsDelegate:(nullable id <BarButtonsDelegate>)delegate;
- (instancetype)initWithCoder:(nullable NSCoder *)aDecoder barButtonsDelegate:(nullable id <BarButtonsDelegate>)delegate;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil
                         bundle:(nullable NSBundle *)nibBundleOrNil
             barButtonsDelegate:(nullable id <BarButtonsDelegate>)delegate;
- (void)handleLeftButtonEvent:(nullable id)sender;
- (void)handleRightButtonEvent:(nullable id)sender;
- (void)setLeftBarButton:(nullable UIButton *)button;
- (void)setLeftBarButtonTitle:(nullable NSString *)text;
- (void)setLeftBarButtonImage:(nullable UIImage *)image;
- (void)setRightBarButton:(nullable UIButton *)button;
- (void)setRightBarButtonTitle:(nullable NSString *)text;
- (void)setRightBarButtonImage:(nullable UIImage *)image;
- (void)setEnableLeftBarButton:(BOOL)enabled;
- (void)setEnableRightBarButton:(BOOL)enabled;
- (void)addTitle:(nullable NSString *)title andSubtitle:(nullable NSString *)subtitle;
- (void)setNavigationBarCustomColor:(nullable UIColor *)color;
- (void)resetDefaultNavigationBarColor;

@end

NS_ASSUME_NONNULL_END
