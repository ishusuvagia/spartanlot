
#import "BaseViewController+Common.h"
#import "SectionTableView.h"
#import "UITools.h"

@implementation BaseViewController (Common)

- (void)startLoading:(SectionTableView *)tableView loadingText:(NSString *)text
{
    if (tableView && !tableView.isRefreshing && !tableView.isFetching) {
        [tableView hideEmptyView];
        [UITools showProgressForView:self.view withText:text ?: LOCALIZED(@"loading_data")];
    }
}

- (void)endLoading:(SectionTableView *)tableView
{
    [UITools hideProgressForView:self.view];
    if (tableView) {
        if ([tableView isRefreshing]) {
            [tableView endRefreshing];
        }
    }
}

- (void)endLoading:(SectionTableView *)tableView error:(NSError *)error
{
    [UITools hideProgressForView:self.view];
    if (tableView) {
        if ([tableView isRefreshing]) {
            [tableView endRefreshing];
        }
        [tableView reload];
    }
    
    [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:error.localizedDescription confirm:nil];
}

@end
