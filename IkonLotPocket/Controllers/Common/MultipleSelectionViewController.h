
#import "ChildViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MultipleSelectionViewController : ChildViewController

@property (nonatomic, weak) id<SelectionDelegate> delegate;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSMutableArray *selectedData;
@property (nonatomic, strong) NSString *navTitle;

@end

NS_ASSUME_NONNULL_END
