
#import "ChildViewController.h"
#import "Constants.h"

@interface ChildViewController () <BarButtonsDelegate>

@end

@implementation ChildViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder barButtonsDelegate:self])) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil barButtonsDelegate:self])) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
}

///--------------------------------------
#pragma mark - <BarButtonsDelegate>
///--------------------------------------

- (UIButton *)setupLeftBarButton
{
    UIButton *backButton = [[UIButton alloc] initWithFrame:
            CGRectMake(0, 0, kBarButtonDefaultWidth, kBarButtonDefaultHeight)];
    [backButton setImage:[UIImage imageNamed:@"icon-back"] forState:UIControlStateNormal];
    return backButton;
}

- (void)handleLeftButtonEvent:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
