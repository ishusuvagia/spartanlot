
#import "EngageMainViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SectionTableView.h"
#import "EnagageListTableViewCell.h"
#import "NavigationController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>

static NSString *const kCellIdentifier = @"EnagageListTableViewCell";
static NSString *const kCellNibName = @"EnagageListTableViewCell";
static CGFloat const kCellSpacing = 5.0f;

@interface EngageMainViewController () <UINavigationControllerDelegate,UIImagePickerControllerDelegate, SectionTableViewDelegate, UITableViewDelegate, RestResponseDelegate>

@property (weak, nonatomic) IBOutlet SectionTableView *tblList;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *segmentControl;

@property (nonatomic, strong) NSString *_currentSortType;
@property (nonatomic, strong) NSMutableDictionary *_currentFilterDict;

@end

@implementation EngageMainViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __currentSortType = kExternal_SortBy_Model;
        __currentFilterDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tblList initWithCellPrototypes:self._prototypes
                           hasHeaderSection:YES
                           hasFooterSection:NO
                              enableRefresh:YES
                                enableFetch:NO
                              emptyDataText:LOCALIZED(@"no_availableitems")
                                   delegate:self];
    self.tblList.tableViewProxyDelegate = self;
    self.tblList.contentInset = UIEdgeInsetsMake(kCellSpacing, 0.0f, 0.0f, 0.0f);
    
    self.segmentControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Dashboard", @"Leads", @"Accounts",@"Task"]];
    [self.segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [self responseHandler];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[@{kCellIdentifierKey: kCellIdentifier, kCellNibNameKey: kCellNibName, kCellClassKey: EnagageListTableViewCell.class}];
}

- (void)_updateTitle:(NSInteger)items
{
    NSString *title = @"Leads";
    
    UIViewController *moduleRootController = self.parentViewController.parentViewController;
    if ([moduleRootController isKindOfClass:NavigationBarViewController.class]) {
        [(NavigationBarViewController *) moduleRootController addTitle:title andSubtitle:@""];
    }
}

///--------------------------------------
#pragma mark - <RestResponseDelegate>
///--------------------------------------

- (void)responseHandler
{
    
    
    [self endLoading:self.tblList];
    [self.tblList reloadData];
    
    [self _updateTitle:15];
    
    NSMutableArray *cellSectionData = [NSMutableArray array];
    NSDictionary *dict = @{
                           kCellIdentifierKey: kCellIdentifier,
                           kCellClassKey: EnagageListTableViewCell.class,
                           kCellHeightKey: @([EnagageListTableViewCell height]),
                           kCellObjectDataKey: @""
                           };
    NSDictionary *section = @{kSectionTitleKey: kEmptyString, kSectionDataKey: @[dict]};
    [cellSectionData addObject:section];
    [self.tblList loadData:cellSectionData];
}

///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------





- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *title, *description, *imageUrl =nil;
    title = @"Eileen Asaro";
    description = @"Referral through current customer";
    imageUrl = @"";



    [(EnagageListTableViewCell *) cell setupCellWithTitle:title description:description imageUrl:@""];
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
//    DeviceEntity *entity = cellDict[kCellObjectDataKey];
//    InventoryDetailViewController *controller = INIT_CONTROLLER_XIB(InventoryDetailViewController);
//    controller.deviceSerialNumber = entity.serialNumber;
//    [self.navigationController pushViewController:controller animated:YES];
}

///--------------------------------------
#pragma mark - <Segment Controll Method>
///--------------------------------------

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld", (long)segmentedControl.selectedSegmentIndex);
}

///--------------------------------------
#pragma mark - <IB Actions>
///--------------------------------------

- (IBAction)addVideoClicked:(id)sender {
    [self setActionsheet];
}

- (IBAction)addImageClicked:(id)sender {
    [self setActionsheet];
}

- (void) setActionsheet {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Capture" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self captureImageOrVideo];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectFromGallery];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}



- (void) captureImageOrVideo {
//    if (_isVideoCapture) {
//        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//            picker.delegate = self;
//            picker.allowsEditing = YES;
//            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
//            [self presentViewController:picker animated:YES completion:NULL];
//        }
//    }
//    else{
//        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Device has no camera" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [myAlertView show];
//        } else {
//            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//            picker.delegate = self;
//            picker.allowsEditing = YES;
//            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            [self presentViewController:picker animated:YES completion:NULL];
//        }
//    }
}

-(void)selectFromGallery {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    if (_isVideoCapture) {
//        self.videoURL = info[UIImagePickerControllerMediaURL];
//        [picker dismissViewControllerAnimated:YES completion:NULL];
//
//        self.videoController = [[MPMoviePlayerController alloc] init];
//
//        [self.videoController setContentURL:self.videoURL];
//        [self.videoController.view setFrame:CGRectMake (0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//        [self.view addSubview:self.videoController.view];
//
//        [self.videoController play];
//    }
//    else{
//        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//        self.thumbnailImage.image = chosenImage;
//        [picker dismissViewControllerAnimated:YES completion:NULL];
//    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    if (error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
//    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
//    // Stop the video player and remove it from view
//    [self.videoController stop];
//    [self.videoController.view removeFromSuperview];
//    self.videoController = nil;
//    // Display a message
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:@"Video Playback" message:@"Just finished the video playback. The video is now removed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
}


@end
