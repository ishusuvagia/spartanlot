
#import "LandingViewController.h"
#import "DashboardView.h"

@interface LandingViewController () <BarButtonsDelegate, RestResponseDelegate>

@property (weak, nonatomic) IBOutlet UIView *_headerView;
@property (weak, nonatomic) IBOutlet UILabel *_dealerNameLbl;
@property (strong, nonatomic) IBOutlet DashboardView *_dashboardView;
@property (weak, nonatomic) IBOutlet UILabel *_poweredByLbl;

@end

@implementation LandingViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder barButtonsDelegate:self])) {
    }
    return self;
}

- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil barButtonsDelegate:self])) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.title = @"asd";//[LOCALIZED(@"title_dashboard") uppercaseString];
//    self._poweredByLbl.text = LOCALIZED(@"poweredby");
//    self._dealerNameLbl.text = AUTHMANAGER.currentDealerName;
//    [UITools dropShadow:self._poweredByLbl shadowOpacity:0.5f shadowRadius:1.0f offset:CGSizeZero];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self._dashboardView initButtons:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueLogin]) {
        BaseViewController *controller = segue.destinationViewController;
        controller.isModal = YES;
    }
}

///--------------------------------------
#pragma mark - <BarButtonsDelegate>
///--------------------------------------

- (UIButton *)setupRightBarButton
{
    return [UITools createBarButtonWithTitle:[LOCALIZED(@"btn_logout") uppercaseString]];
}

- (void)handleRightButtonEvent:(nullable id)sender
{
    [AUTHMANAGER logout];
    [self performSegueWithIdentifier:kSegueLogin sender:self];
}

@end
