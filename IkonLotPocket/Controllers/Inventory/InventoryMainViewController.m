
#import "InventoryMainViewController.h"
#import "InventoryListViewController.h"
#import "SectionTableView.h"
#import "RoundTableCell.h"

static NSString *const kCellIdentifier = @"roundtablecell";
static NSString *const kCellNibName = @"RoundTableCell";

@interface InventoryMainViewController () <SectionTableViewDelegate>

@property (weak, nonatomic) IBOutlet SectionTableView *_tableView;
@property (nonatomic, strong) InventoryListViewController *_inventoryListViewController;

@end

@implementation InventoryMainViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self._tableView initWithCellPrototypes:self._prototypes
                           hasHeaderSection:YES
                           hasFooterSection:NO
                              enableRefresh:NO
                                enableFetch:NO
                              emptyDataText:kEmptyString
                                   delegate:self];
    [self _setupData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self _updateTitle];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[@{kCellIdentifierKey: kCellIdentifier, kCellNibNameKey: kCellNibName, kCellClassKey: RoundTableCell.class}];
}

- (void)_setupData
{
    NSMutableArray *cellSectionData = [NSMutableArray array];
    NSDictionary *newVehiclesSection = @{kSectionTitleKey: kEmptyString, kSectionDataKey: @[@{
            kCellIdentifierKey: kCellIdentifier,
            kCellClassKey: RoundTableCell.class,
            kCellHeightKey: @([RoundTableCell height]),
            kCellColorCodeKey: [AppColorScheme brandGreen],
            kCellInformationKey: LOCALIZED(@"newvehicles")
    }]};
    [cellSectionData addObject:newVehiclesSection];
    NSDictionary *usedVehiclesSection = @{kSectionTitleKey: kEmptyString, kSectionDataKey: @[@{
            kCellIdentifierKey: kCellIdentifier,
            kCellClassKey: RoundTableCell.class,
            kCellHeightKey: @([RoundTableCell height]),
            kCellColorCodeKey: [AppColorScheme brandOrange],
            kCellInformationKey: LOCALIZED(@"usedvehicles")
    }]};
    [cellSectionData addObject:usedVehiclesSection];
    

    [self._tableView loadData:cellSectionData];
}

- (void)_addInventoryListView:(NSString *)type
{
    self._inventoryListViewController = INIT_CONTROLLER_XIB(InventoryListViewController);
    
    if ([type isEqualToString:LOCALIZED(@"newvehicles")]) {
        self._inventoryListViewController.vehicleCondtion = @"new";
    } else if ([type isEqualToString:LOCALIZED(@"usedvehicles")]) {
        self._inventoryListViewController.vehicleCondtion = @"used";
    }
    
    self._tableView.hidden = YES;
    self._inventoryListViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self addChildViewController:self._inventoryListViewController];
    [self.view addSubview:self._inventoryListViewController.view];
}

- (void)_updateTitle
{
    NSString *subtitle = kEmptyString;
    if (AUTHMANAGER.totalFleetUnits == 1) {
        subtitle = [NSString stringWithFormat:@"%ld %@", (long) AUTHMANAGER.totalFleetUnits, LOCALIZED(@"vehicle")];
    } else {
        subtitle = [NSString stringWithFormat:@"%ld %@", (long) AUTHMANAGER.totalFleetUnits, LOCALIZED(@"vehicles")];
    }
    if ([self.parentViewController isKindOfClass:NavigationBarViewController.class]) {
        [(NavigationBarViewController *) self.parentViewController addTitle:LOCALIZED(@"title_inventory") andSubtitle:subtitle];
    }
}

///--------------------------------------
#pragma mark - <ScrollableTabbarDelegate>
///--------------------------------------

- (void)handleLeftNavigationBarButtonEvent
{
    [self _updateTitle];
    [self._inventoryListViewController.view removeFromSuperview];
    [self._inventoryListViewController removeFromParentViewController];
    self._inventoryListViewController = nil;
    self._tableView.hidden = NO;
}

///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------

- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = cellDict[kCellInformationKey];
    UIColor *colorCode = cellDict[kCellColorCodeKey];
    [(RoundTableCell *) cell setupCellWithTitle:title colorCode:colorCode];
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = cellDict[kCellInformationKey];
    [self _addInventoryListView:title];
}

@end
