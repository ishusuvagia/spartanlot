//
//  InventoryDetailsNewViewController.h
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "ChildViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryDetailsNewViewController : ChildViewController
{
    IBOutlet UIScrollView *ImgPagination;
    IBOutlet UIPageControl *pageControl;
    BOOL pageControlBeingUsed;
    IBOutlet UIButton *statsBtn,*featuresBtn,*locationBtn;

}
@property (nonatomic, strong) NSString *deviceSerialNumber;


@end

NS_ASSUME_NONNULL_END
