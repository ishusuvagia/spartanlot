
#import "InventoryListViewController.h"
#import "InventoryDetailViewController.h"
#import "InventorySortFilterViewController.h"
#import "NavigationController.h"
#import "SectionTableView.h"
#import "DeviceService.h"
#import "InventoryService.h"
#import "InventoryTableCell.h"
#import "InventoryMainViewController.h"
#import "InventoryDetailsNewViewController.h"
#import "LandingViewController.h"

static NSString *const kCellIdentifier = @"inventorytablecell";
static NSString *const kCellNibName = @"InventoryTableCell";
static CGFloat const kCellSpacing = 5.0f;

@interface InventoryListViewController () <RestResponseDelegate, SectionTableViewDelegate, UITableViewDelegate, UISearchBarDelegate, InventorySortFilterDelegate>
{
    BOOL _isRequesting;
}

@property (weak, nonatomic) IBOutlet UISearchBar *_searchBar;
@property (weak, nonatomic) IBOutlet SectionTableView *_tableView;

@property (nonatomic, strong) NSString *_currentSortType;
@property (nonatomic, strong) NSMutableDictionary *_currentFilterDict;

@end

@implementation InventoryListViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __currentSortType = kExternal_SortBy_Model;
        __currentFilterDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(Search_friend_tf.frame.size.width,-2, 15, 15)];
    imgView.image = [UIImage imageNamed:@"icon-search"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [paddingView addSubview:imgView];
    [Search_friend_tf setLeftViewMode:UITextFieldViewModeAlways];
    [Search_friend_tf setLeftView:paddingView];
    Search_friend_tf.layer.cornerRadius = 2;
    Search_friend_tf.clipsToBounds=YES;
    Search_friend_tf.backgroundColor =[AppColorScheme white];
    
    [self._tableView setBackgroundColor:[UIColor clearColor]];
    [self._tableView initWithCellPrototypes:self._prototypes
                           hasHeaderSection:YES
                           hasFooterSection:NO
                              enableRefresh:YES
                                enableFetch:NO
                              emptyDataText:LOCALIZED(@"no_availableitems")
                                   delegate:self];
    self._tableView.tableViewProxyDelegate = self;
    self._tableView.contentInset = UIEdgeInsetsMake(kCellSpacing, 0.0f, 0.0f, 0.0f);
    [UITools dropShadow:self._searchBar shadowOpacity:0.5f shadowRadius:1.0f offset:CGSizeZero];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self _requestData];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[@{kCellIdentifierKey: kCellIdentifier, kCellNibNameKey: kCellNibName, kCellClassKey: InventoryTableCell.class}];
}

- (void)_updateTitle:(NSInteger)items
{
    NSString *title = [self.vehicleCondtion isEqualToString:@"new"] ? LOCALIZED(@"newvehicles") : LOCALIZED(@"usedvehicles");
    
    NSString *subtitle = kEmptyString;
    if (items == 1) {
        subtitle = [NSString stringWithFormat:@"%ld %@", (long) items, LOCALIZED(@"vehicle")];
    } else {
        subtitle = [NSString stringWithFormat:@"%ld %@", (long) items, LOCALIZED(@"vehicles")];
    }
    
    UIViewController *moduleRootController = self.parentViewController.parentViewController;
    if ([moduleRootController isKindOfClass:NavigationBarViewController.class]) {
        [(NavigationBarViewController *) moduleRootController addTitle:title andSubtitle:subtitle];
    }
}

- (void)_requestData
{
    if (_isRequesting) return;

    [self startLoading:self._tableView loadingText:LOCALIZED(@"loading_data")];
    _isRequesting = YES;

    DeviceService *service = GET_SERVICE(DeviceService);
    [service getDeviceListByFleetId:AUTHMANAGER.fleetId totalFleetUnits:AUTHMANAGER.totalFleetUnits
                          condition:self.vehicleCondtion sortType:self._currentSortType
                       isRefreshing:self._tableView.isRefreshing responder:self];
}

- (void)_searchInventory:(NSString *)searchText
{
    DeviceService *service = GET_SERVICE(DeviceService);
    [service searchDevice:searchText fleetId:AUTHMANAGER.fleetId condition:self.vehicleCondtion responder:self];
}

///--------------------------------------
#pragma mark - <RestResponseDelegate>
///--------------------------------------

- (void)responseHandler:(RLMResults *)results
{
    _isRequesting = NO;
    [self endLoading:self._tableView];
    [self._tableView reloadData];
    
    [self _updateTitle:results.count];
    
    NSMutableArray *cellSectionData = [NSMutableArray array];
    for (DeviceEntity *entity in results) {
        NSDictionary *dict = @{
                kCellIdentifierKey: kCellIdentifier,
                kCellClassKey: InventoryTableCell.class,
                kCellHeightKey: @([InventoryTableCell height]),
                kCellObjectDataKey: entity
        };
        NSDictionary *section = @{kSectionTitleKey: kEmptyString, kSectionDataKey: @[dict]};
        [cellSectionData addObject:section];
    }
    TotalCountLbl.text =[NSString stringWithFormat:@"   %lu Result Found",[cellSectionData count]];
    [self._tableView loadData:cellSectionData];
}

- (void)errorHandler:(NSError *)error
{
    _isRequesting = NO;
    [self endLoading:self._tableView error:error];
}

///--------------------------------------
#pragma mark - <ScrollableTabbarDelegate>
///--------------------------------------

- (void)handleNavigationBarButtonEvent
{
    InventorySortFilterViewController *controller = INIT_CONTROLLER_XIB(InventorySortFilterViewController);
    controller.currentSortType = self._currentSortType;
    controller.currentFilterDict = self._currentFilterDict;
    controller.delegate = self;
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
}

///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------

- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    DeviceEntity *deviceEntity = cellDict[kCellObjectDataKey];
    InventoryEntity *vehicleEntity = deviceEntity.vehicle;

    NSString *title, *condition, *inventoryCode, *exteriorColor, *interiorColor, *vin, *imageUrl = nil;
    CGFloat msrpPrice, mileage = 0.0f;
    NSString *batteryLevel = deviceEntity.batteryLevel;
    BOOL deviceInstalled = deviceEntity.imei && deviceEntity.isInstalled && [deviceEntity.simStatus isEqualToString:@"ACTIVED"];

    if (vehicleEntity.inventoryId != nil) {
        title = vehicleEntity.getInventoryTitle;
        condition = vehicleEntity.getConditionStatus;
        inventoryCode = vehicleEntity.stockNumber;
        vin = vehicleEntity.vin;
        msrpPrice = vehicleEntity.msrp;
        mileage = vehicleEntity.miles;
        exteriorColor = vehicleEntity.exteriorColor;
        interiorColor = vehicleEntity.interiorColor;
        if (vehicleEntity.photoLinks.count > 0) {
            imageUrl = [[vehicleEntity.photoLinks objectAtIndex:0] stringValue];
        }
    } else {
        title = deviceEntity.getVehicleInventoryTitle;
        vin = deviceEntity.vin;
        inventoryCode = deviceEntity.stockNumber;
        msrpPrice = mileage = 0.0f;
        imageUrl = [NSString stringWithFormat:@"%@%@", kSkyPatrolAvatarImageRepoUrl, deviceEntity.avatar];
    }
    [(InventoryTableCell *) cell setupCellWithTitle:title condition:condition
                                      inventoryCode:inventoryCode vin:vin
                                          msrpPrice:msrpPrice mileage:mileage
                                      exteriorColor:exteriorColor interiorColor:interiorColor
                                    deviceInstalled:deviceInstalled
                                       batteryLevel:batteryLevel
                                           imageUrl:imageUrl
                                            condition:condition];
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
//    DeviceEntity *entity = cellDict[kCellObjectDataKey];
//    InventoryDetailViewController *controller = INIT_CONTROLLER_XIB(InventoryDetailViewController);
//    controller.deviceSerialNumber = entity.serialNumber;
//    [self.navigationController pushViewController:controller animated:YES];
    DeviceEntity *entity = cellDict[kCellObjectDataKey];
    InventoryDetailsNewViewController *controller = INIT_CONTROLLER_XIB(InventoryDetailsNewViewController);
    controller.deviceSerialNumber = entity.serialNumber;
    [self.navigationController pushViewController:controller animated:YES];

}

- (void)refreshData
{
    [self _requestData];
}

///--------------------------------------
#pragma mark - <UITableViewDelegate>
///--------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
///--------------------------------------
#pragma mark - <UISearchBarDelegate>
///--------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    TRACE("searchText=%@", searchText);
    [self _searchInventory:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:YES];
    searchBar.text = nil;
    [self _requestData];
}

///--------------------------------------
#pragma mark - <InventorySortFilterDelegate>
///--------------------------------------

- (void)updateSortString:(NSString *)string filterDict:(NSDictionary *)filterDict
{
    self._currentSortType = string;
    [self._currentFilterDict addEntriesFromDictionary:filterDict];
    [self refreshData];
}
-(IBAction)sortingBtnPressed:(id)sender
{
    if([shortingBtn isSelected])
    {
        [sender setSelected:NO];
        [shortingBtn setBackgroundImage:[UIImage imageNamed:@"inventory-sort"] forState:UIControlStateNormal];
        if (_isRequesting) return;
        [self startLoading:self._tableView loadingText:LOCALIZED(@"loading_data")];
        _isRequesting = YES;
        DeviceService *service = GET_SERVICE(DeviceService);
        [service getDeviceListByFleetId:AUTHMANAGER.fleetId totalFleetUnits:AUTHMANAGER.totalFleetUnits
                              condition:@"new" sortType:self._currentSortType
                           isRefreshing:self._tableView.isRefreshing responder:self];
    }
    else
    {
        [shortingBtn setBackgroundImage:[UIImage imageNamed:@"inventory-resort"] forState:UIControlStateNormal];
        if (_isRequesting) return;
        
            [self startLoading:self._tableView loadingText:LOCALIZED(@"loading_data")];
            _isRequesting = YES;
        
            DeviceService *service = GET_SERVICE(DeviceService);
            [service getDeviceListByFleetId:AUTHMANAGER.fleetId totalFleetUnits:AUTHMANAGER.totalFleetUnits
                                  condition:@"used" sortType:self._currentSortType
                               isRefreshing:self._tableView.isRefreshing responder:self];
        [sender setSelected:YES];
    }
}
-(IBAction)HomeBtnPressed:(id)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMain bundle:nil];
    UIViewController *initialViewController = nil;
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:kSceneLanding];
    initialViewController = [[NavigationController alloc] initWithRootViewController:viewController];
    self.window.rootViewController = initialViewController;
}

@end
